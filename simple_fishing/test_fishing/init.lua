local testroddef = {
    description = "sdf",
    uses = 32,
    chance = 0.1,
    treasure_chance = 0.5,
    length = 15,
    texture = "dfg",
}

local fishdef = {
    name = "default:apple",
    ymin = -31000,
    ymax = 31000,
    heatmin = 0,
    heatmax = 100,
}

local treasuredef = {
    name = "default:cobblestone",
    ymin = -31000,
    ymax = 31000,
    heatmin = 0,
    heatmax = 100,
}

simple_fishing.register_fishing_rod("test_fishing:rod", testroddef)
simple_fishing.register_fish(fishdef)
simple_fishing.register_fish(treasuredef, true)
