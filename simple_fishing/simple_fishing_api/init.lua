-------------------------------------------------------------------------------
--Default data structures
-------------------------------------------------------------------------------

simple_fishing = {}

local common_fish = {}
local treasures = {}
local active_hooks = {}

-------------------------------------------------------------------------------
--Internal variables
-------------------------------------------------------------------------------

--TODO: replace all hardcoded strings with modname
--TODO: fix physics
local modname = "simple_fishing"
local drop_force = 5

-------------------------------------------------------------------------------
--Settingtypes
-------------------------------------------------------------------------------

local function get_setting(name, def, bool)

    if bool then
        local inp = minetest.settings:get_bool(modname .. "_" .. name, def)
        return inp
    else
        local inp = minetest.settings:get(modname .. "_" .. name) or def
        return tonumber(inp)
    end

end

local valid_depth = get_setting("min_depth", 4)
local valid_distance = get_setting("max_distance", 24)
local bob_timeout = get_setting("bob_timeout", 5)
local update_interval = get_setting("interval", 1)
local global_ymin = get_setting("global_ymin", -31000)
local global_ymax = get_setting("global_ymax", 31000)
local global_heatmin = get_setting("global_heatmin", 0)
local global_heatmax = get_setting("global_heatmax", 100)
local ent_ray_dist = get_setting("raycast_dist", 1)

-------------------------------------------------------------------------------
--Utilities
-------------------------------------------------------------------------------

local function msg(level, input)
    minetest.log(level, "[" .. modname .. "] " .. input)
end

local function find_free_id()
    --Should never occur
    for k,v in pairs(active_hooks) do
        if k ~= 0 and not v then return k end
    end

    return #active_hooks + 1
end

local function fastpow(a, b)
    local state = a
    for i = 1, b - 1 do
        state = state * a
    end
    return state
end

local function distance(pos1, pos2)
    return math.sqrt(
    fastpow(pos1.x - pos2.x, 2) +
    fastpow(pos1.y - pos2.y, 2) +
    fastpow(pos1.z - pos2.z, 2)
    )
end

local function spawn_bob_effects(pos)
    minetest.add_particlespawner({
        amount = 8,
        time = 0.05,
        minpos = {x=pos.x-0.5, y=pos.y, z=pos.z-0.5},
        maxpos = {x=pos.x+0.5, y=pos.y, z=pos.z+0.5},
        minvel = {x=0, y=0, z=0},
        maxvel = {x=0, y=0, z=0},
        minacc = {x=0, y=-9.8, z=0},
        maxacc = {x=0, y=-9.8, z=0},
        minexptime = 3,
        maxexptime = 3,
        minsize = 1,
        maxsize = 3,
        collisiondetection = true,
        collision_removal = true,
        texture = "image.png",
    })
end

local function spawn_caught_effects(pos)
    minetest.add_particlespawner({
        amount = 24,
        time = 0.05,
        minpos = {x=pos.x-0.5, y=pos.y, z=pos.z-0.5},
        maxpos = {x=pos.x+0.5, y=pos.y, z=pos.z+0.5},
        minvel = {x=0, y=0, z=0},
        maxvel = {x=0, y=0, z=0},
        minacc = {x=0, y=-9.8, z=0},
        maxacc = {x=0, y=-9.8, z=0},
        minexptime = 3,
        maxexptime = 3,
        minsize = 1,
        maxsize = 3,
        collisiondetection = true,
        collision_removal = true,
        texture = "image.png",
    })
    minetest.sound_play("simple_fishing_caught",
    {gain = 1.0, pitch = 1.0}, true)
end

-------------------------------------------------------------------------------
--API helpers
-------------------------------------------------------------------------------

local function invalidate_hook(luaentity, stack)
    if stack then
        local meta = stack:get_meta()
        if stack:get_definition()._fishing then
            meta:set_int("active", 0)
            meta:set_int("hook_id", 0)
        end
    end

    for k,v in pairs(active_hooks) do
        if k == luaentity._id then
            print(k)
            table.remove(active_hooks, k)
            break
        end
    end

    luaentity.object:remove()
end

local function select_fitting_catch(pos, treasure)

    local heat = minetest.get_heat(pos)
    local source = common_fish
    local fitting = {}

    if treasure then
        source = treasures
    end

    for _,v in pairs(source) do
        if pos.y > v.ymin and pos.y < v.ymax and
        heat > v.heatmin and heat < v.heatmax then
            table.insert(fitting, v.name)
        end
    end

    if #fitting == 0 then
        return nil
    end

    return fitting[math.random(#fitting)]
end

local function test_water(pos)
    local depth = 0
    for i = 0,valid_depth,1 do
        local node = minetest.get_node_or_nil({
            x = pos.x,
            y = pos.y - i - 1,
            z = pos.z
        })
        if node and node.name == "default:water_source" then
            depth = depth + 1
        end
    end
    return depth == valid_depth
end

local function add_to_inventory_or_drop(itemname, user)

    local inventory = user:get_inventory()

    if inventory:room_for_item("main", {name = itemname}) then
        inventory:add_item("main", {name = itemname})
    else
        local pos = user:get_pos()
        local dir = user:get_look_dir()
        local obj = minetest.add_item({
            x = pos.x,
            y = pos.y + 1.5,
            z = pos.z,
        }, itemname)

        obj:setvelocity({
            x = dir.x * drop_force,
            y = dir.y * drop_force,
            z = dir.z * drop_force,
        })

    end

end

local function process_collisions(obj)

    local pos = obj:get_pos()
    local node = minetest.get_node(pos)
    local nodedef = minetest.registered_nodes[node.name]
    local luaent = obj:get_luaentity()

    local previous_pos = luaent._previous_pos
    if not previous_pos then
        local ownerpos = luaent._owner:get_pos()
        previous_pos = {x = ownerpos.x, y = ownerpos.y + 1.5, z = ownerpos.z}
    end

    local delta = {
        x = pos.x - previous_pos.x,
        y = pos.y - previous_pos.y,
        z = pos.z - previous_pos.z,
    }

    local ray = minetest.raycast(
    pos,
    {
        x = pos.x + delta.x * ent_ray_dist,
        y = pos.y + delta.y * ent_ray_dist,
        z = pos.z + delta.z * ent_ray_dist,
    },
    false,
    false)

    local collision = ray:next()
    local stuck = false

    if collision and collision.type == "node" then
        stuck = true
    end

    if stuck then
        obj:set_velocity({x=0,y=0,z=0})
        obj:set_acceleration({x=0,y=0,z=0})
    elseif nodedef and nodedef.liquidtype ~= "none" then
        if pos.y > math.floor(pos.y) then
            obj:set_velocity({x=0,y=0.5,z=0})
            obj:set_acceleration({x=0,y=0,z=0})
        end
    else
        obj:set_acceleration({x = 0, y = -10, z = 0})
    end

    luaent._previous_pos = pos
end

local function entity_on_step(self, dtime, moveresult)

    local obj = self.object

    process_collisions(obj)

    self._timer = self._timer + dtime
    if self._timer < update_interval then return end

    --FIXME: actually test with get_connected_players() because reasons
    --FIXME: or just sanitize getpos
    local owner = self._owner

    --remove if owner is not valid
    if not minetest.is_player(self._owner) then invalidate_hook(self) end

    local stack = owner:get_wielded_item()

    --remove if no itemstack is wielded
    if not stack then
        invalidate_hook(self)
     end

    local meta = stack:get_meta()
    local hook_id = meta:get_int("hook_id")

    --Remove if wielded itemstack is different
    if hook_id == 0 or hook_id ~= self._id then
        invalidate_hook(self)
    end

    --remove if too far away
    if distance(obj:get_pos(), owner:get_pos()) > valid_distance then
        invalidate_hook(self)
    end

    --randomly start bobbing if in water
    if math.random() < self._chance and test_water(obj:get_pos()) then
        self._bob = true
    end

    --bobbing
    if self._bob then

        self._bob_timer = self._bob_timer + dtime
        spawn_bob_effects(obj:get_pos())
        obj:setvelocity({x = 0, y = 1, z = 0})

        if self._bob_timer > bob_timeout then
            self._bob_timer = 0
            self._bob = false
        end

    end

    self._timer = 0

end

local function tool_on_use(itemstack, user, pos)

    local meta = itemstack:get_meta()
    local pos = user:get_pos()

    local itemdef = itemstack:get_definition()

    --Reset active metadata if no object is present
    --(a user might have disconnected while fishing)
    if meta:get_int("active") == 1 then

        local test_id = meta:get_int("hook_id")

        if test_id ~= 0 and not active_hooks[test_id] then
            meta:set_int("active", 0)
            meta:set_int("hook_id", 0)
        end
    end

    local active = meta:get_int("active")

    if active == 1 then

        local obj = active_hooks[meta:get_int("hook_id")]

        if obj then

            local luaent = obj:get_luaentity()

            if luaent._bob then

                local treasure = math.random() < itemdef._chance
                local item = select_fitting_catch(user:get_pos(), treasure)

                if item then
                    add_to_inventory_or_drop(item, user)
                    spawn_caught_effects(obj:get_pos())
                end
            end

            invalidate_hook(obj:get_luaentity(), itemstack)
        end

        return itemstack

    elseif active == 0 then

        local wear = itemstack:get_wear()

        wear = wear + (65535/itemdef._uses)

        itemstack:set_wear(wear)
        if wear > 65535 then return end

        local dir = user:get_look_dir()
        local obj = minetest.add_entity({
            x = pos.x + dir.x * 2,
            y = pos.y + 1.5 + dir.y * 2,
            z = pos.z + dir.z * 2
        },
        "simple_fishing_api:hook")

        local luaent = obj:get_luaentity()
        local id = find_free_id()

        luaent._owner = user
        luaent._id = id
        luaent._chance = itemdef._chance

        active_hooks[id] = obj

        obj:setvelocity({
            x = dir.x * itemdef._length,
            y = dir.y * itemdef._length,
            z = dir.z * itemdef._length,
        })

        meta:set_int("active", 1)
        meta:set_int("hook_id", id)

        return itemstack
    end
end

local function tool_on_drop(itemstack, dropper, pos)
    local meta = itemstack:get_meta()
    local active = meta:get_int("active")
    local id = meta:get_int("hook_id")

    if active == 1 and id ~= 0 then
        invalidate_hook(active_hooks[id].get_luaentity(), itemstack)
    end

    return minetest.item_drop(itemstack, dropper, pos)
end

-------------------------------------------------------------------------------
--API
-------------------------------------------------------------------------------

simple_fishing.register_fishing_rod = function(name, def)

    if not name or not def.description or not def.chance or
    not def.treasure_chance or not def.length or not def.uses then
        msg("error", "Missing fields in " .. name .. " registration!")
        return
    end

    if minetest.registered_tools[name] then
        msg("error", "Tool " .. name .. " is already registered!")
        return
    end

    minetest.register_tool(name, {
        description = def.description,
        inventory_image = def.texture,
        wield_image = def.texture,
        wield_scale = {x = 1, y = 1, z = 1},
        range = 4.0,
        liquids_pointable = false,
        _chance = def.chance,
        _treasure_chance = def.treasure_chance,
        _uses = def.uses,
        _length = def.length,
        _fishing = true,
        on_drop = tool_on_drop,
        on_use = tool_on_use,
    })
end

simple_fishing.register_fish = function(def, treasure)

    if not def.name then
        msg("error", "Missing fields in fish definition!")
        return
    end

    local tmpdef = {
        name = def.name,
        ymin = def.ymin or global_ymin,
        ymax = def.ymax or global_ymax,
        heatmin = def.heatmin or global_heatmin,
        heatmax = def.heatmadx or global_heatmax,
    }

    if treasure then
        table.insert(treasures, tmpdef)
    else
        table.insert(common_fish, tmpdef)
    end

end

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

minetest.register_entity("simple_fishing_api:hook", {
    initial_properties = {
        physical = false,
        collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},
        pointable = true,
        visual = "sprite",
        visual_size = {x = 1, y = 1, z = 1},
        textures = {"simple_fishing_hook.png"},
        is_visible = true,
        static_save = false,
        shaded = true,
    },
    on_step = entity_on_step,
    _owner = nil,
    _timer = 0,
    _id = nil,
    _chance = nil,
    _bob = false,
    _bob_timer = 0,
    _previous_pos = nil,
    })
