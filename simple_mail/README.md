# License
All code is licensed under AGPL-3.0-only [link to the license](https://spdx.org/licenses/AGPL-3.0-only.html).  
All resources are licensed under CC 4.0 Attribution-NonCommercial-NoDerivs 4.0 International (CC BY-NC-ND 4.0) [link to the license](https://creativecommons.org/licenses/by-nc-nd/4.0/).  
