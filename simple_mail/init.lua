-------------------------------------------------------------------------------
--Internal Data structures
-------------------------------------------------------------------------------

local mail = {}
local player_context = {}

-------------------------------------------------------------------------------
--Settingtype initialization
-------------------------------------------------------------------------------

local mod_name = "mail"

local function get_setting(name, def, bool)

    if bool then
        local inp = minetest.settings:get_bool(mod_name .. "_" .. name, def)
        return inp
    else
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return tonumber(inp)
    end

end

local enable_autosave = get_setting("enable_autosave", true, true)
local autosave_interval = get_setting("autosave_interval", 60)
local require_paper = get_setting("require_paper", true, false)
local max_inbox = get_setting("max_messages", 50)
local max_topic_size = get_setting("max_topic_size", 32)
local max_message_size = get_setting("max_message_size", 1000)

-------------------------------------------------------------------------------
--Internal variables
-------------------------------------------------------------------------------

local from_colors = {"#0080FF", "#707070"}
local topic_colors = {"#3090FF", "#909090"}
local draft_colors = {"#309090", "#60AAAA"}
local column_colors = {"#80FFA0", "#8080FF"}
local notification_color = "#FF8050"
local message_color = "#50FF80"

-------------------------------------------------------------------------------
--Utilities
-------------------------------------------------------------------------------

local function msg(level, input)
    minetest.log(level, "[" .. mod_name .. "] " .. input)
end

-------------------------------------------------------------------------------
--Saving and loading
-------------------------------------------------------------------------------

local function load_mail()

    local file = io.open(minetest.get_worldpath() ..
    "/" ..
    mod_name ..
    ".mt", "r")

    if file then
        local rawfile = file:read()
        io.close(file)
        if rawfile then
            mail = minetest.deserialize(rawfile)
        else
            msg("error", "Unable to read mail data!")
        end
    end

end

local function save_mail()

    local path = minetest.get_worldpath() ..
    "/" ..
    mod_name ..
    ".mt"

    local file = io.open(path, "w")

    if file then
        local rawfile = minetest.serialize(mail)
        file:write(rawfile)
        io.close(file)
    else
        msg("error", "Unable to write mail data!")
    end

end

local timer = 0
if enable_autosave then

    minetest.register_globalstep(function(dtime)

            if timer > autosave_interval then
                if minetest.get_connected_players() == nil then return end
                save_mail()
                timer = 0
                return
            end

            timer = timer + dtime
    end)

end

minetest.register_on_shutdown(save_mail)
load_mail()

-------------------------------------------------------------------------------
--Formspec building
-------------------------------------------------------------------------------

local function build_formspec(player)

    local name = player:get_player_name()

    local inbox_list = {column_colors[1], "From", column_colors[2], "Topic"}
    local draft_list = {column_colors[1], "To", column_colors[2], "Topic"}

    for k,v in pairs(mail[name].inbox) do
        inbox_list[#inbox_list+1] = from_colors[v.seen]
        inbox_list[#inbox_list+1] = minetest.formspec_escape(v.from)
        inbox_list[#inbox_list+1] = topic_colors[v.seen]
        inbox_list[#inbox_list+1] = minetest.formspec_escape(v.topic)
    end

    for k,v in pairs(mail[name].drafts) do
        draft_list[#draft_list+1] = draft_colors[1]
        draft_list[#draft_list+1] = minetest.formspec_escape(v.to)
        draft_list[#draft_list+1] = draft_colors[2]
        draft_list[#draft_list+1] = minetest.formspec_escape(v.topic)
    end

    local origin = ""
    local topic = ""
    local message = " "

    if not player_context[name].clear and
    not player_context[name].compositing and player_context[name].inbox_id then

        local current_message = mail[name].inbox[player_context[name].inbox_id]

        if current_message then
            origin = current_message.from
            topic = current_message.topic
            message = current_message.content

            if current_message.seen == 1 then
                mail[name].inbox[player_context[name].inbox_id].seen = 2
            end

        end

    elseif not player_context[name].clear and
    player_context[name].draft_id then

        local current_message = mail[name].drafts[player_context[name].draft_id]

        if current_message then
            origin = current_message.to
            topic = current_message.topic
            message = current_message.content
        end

    end

    if player_context[name].clear then player_context[name].clear = nil end

    local formspec = {}

    if player_context[name].compositing then

        formspec[#formspec + 1] = "button[6.52,7.9;1.5,1;"
        formspec[#formspec + 1] = mod_name
        formspec[#formspec + 1] = "_send_button;Send]"

        formspec[#formspec + 1] = "button[4.61,7.9;1.5,1;"
        formspec[#formspec + 1] = mod_name
        formspec[#formspec + 1] = "_save_draft;Save Draft]"

    else
        formspec[#formspec + 1] = "box[3.21,2.825;4.6,4.97;#00000025]"

        formspec[#formspec + 1] = "button[3.21,7.9;1.5,1;"
        formspec[#formspec + 1] = mod_name
        formspec[#formspec + 1] = "_new_mail;New Mail]"

    end

    formspec[#formspec + 1] = "field_close_on_enter["
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_to_from_field;false]"

    formspec[#formspec + 1] = "field_close_on_enter["
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_topic_field;false]"

    formspec[#formspec + 1] = "button[3.21,7.9;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_new_mail;New Mail]"

    formspec[#formspec + 1] = "label[0,-0.25;Inbox:]"
    formspec[#formspec + 1] = "label[0,4.55;Drafts:]"

    formspec[#formspec + 1] = "tablecolumns["
    formspec[#formspec + 1] = "color,span=1;text,align=left,width=4;"
    formspec[#formspec + 1] = "color,span=1;text,align=left,width=4]"

    formspec[#formspec + 1] = "table[0,0.2;3,3.5;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_inbox_table;"
    formspec[#formspec + 1] = table.concat(inbox_list, ",")
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "tablecolumns["
    formspec[#formspec + 1] = "color,span=1;text,align=left,width=4;"
    formspec[#formspec + 1] = "color,span=1;text,align=left,width=4]"
    formspec[#formspec + 1] = "table[0,5;3,2.8;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_drafts_table;"
    formspec[#formspec + 1] = table.concat(draft_list, ",")
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "field[3.5,0.435;4.8,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_to_from_field;To/From:;"
    formspec[#formspec + 1] = minetest.formspec_escape(origin)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "field[3.5,1.555;4.8,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_topic_field;Topic:;"
    formspec[#formspec + 1] = minetest.formspec_escape(topic)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "button[0,3.79;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_inbox_delete_button;Delete]"

    formspec[#formspec + 1] = "button[0,7.9;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_draft_delete_button;Delete]"

    formspec[#formspec + 1] = "textarea[3.5,2.8;4.8,5.9;"

    if player_context[name].compositing then
        formspec[#formspec + 1] = mod_name
        formspec[#formspec + 1] = "_message_area"
    end

    formspec[#formspec + 1] = ";Message:;"
    formspec[#formspec + 1] = minetest.formspec_escape(message)
    formspec[#formspec + 1] = "]"

    return table.concat(formspec)
end

-------------------------------------------------------------------------------
--Utils
-------------------------------------------------------------------------------

local function update_formspec(player)
    if not player then return end
    if sfinv.get_page(player) == mod_name .. ":mail" then
        sfinv.set_page(player, mod_name .. ":mail")
    end
end

local function get_mail_formspec(self, player, context)
    return sfinv.make_formspec(player,
    context,
    build_formspec(player),
    false,
    "size[8,8.6]")
end

local function user_message(name, message)

    local message = minetest.colorize(message_color,
    "<" .. mod_name .. "> " .. message)

    minetest.chat_send_player(name, message)
end

local function user_notification(name, message)

    local message = minetest.colorize(notification_color,
    "<" .. mod_name .. "> " .. message)

    minetest.chat_send_player(name, message)

    minetest.sound_play(mod_name .. "_notification",
    {
        to_player = name,
        gain = 1
    })

end

local function get_unread_amount(name)

    if not mail[name] then return end

    local unread = 0

    for k,v in pairs(mail[name].inbox) do
        if v.seen == 1 then unread = unread + 1 end
    end

    return unread
end

local function send_mail(player, recipient, topic, message, force)

    local name = player:get_player_name()

    if not recipient or recipient == "" then
        user_message(name, "Please specify the recipient!")
        return
    end

    if not topic or topic == "" then
        topic = "No Topic"
    end

    if not message or message == "" then
        user_message(name, "You can't send an empty mail!")
        return
    end

    if not force and string.len(message) >= max_message_size then
        user_message(name,
        "Maximum message size is " .. max_message_size .. " chars!")
        return
    end

    if not force and string.len(topic) >= max_topic_size then
        user_message(name,
        "Maximum topic size is " .. max_topic_size .. " chars!")
        return
    end

    if not mail[recipient] then

        if minetest.player_exists(recipient) then
            mail[recipient] = {inbox = {}, drafts = {}}
        else
            user_message(name, "Specified user doesn't exist!")
            return
        end

    end

    if not force and #mail[recipient].inbox >= max_inbox then
        user_message(name, "Can't send the message! Recipient inbox is full!")
        return
    end

    if require_paper then

        local inv = player:get_inventory()

        if not inv:contains_item("main", {name = mod_name .. ":envelope"}) then
            user_message(name, "You need an envelope to send a mail!")
            return
        end

        inv:remove_item("main", {name = mod_name .. ":envelope"})

    end

    table.insert(mail[recipient].inbox,
    {
        from = name,
        topic = topic,
        content = message,
        seen = 1
    })

    update_formspec(player)

    update_formspec(minetest.get_player_by_name(recipient))

    user_message(player:get_player_name(),
    "Sent message to " .. recipient .. "!")

    user_notification(recipient,
    "You've received a new mail! You have " ..
    get_unread_amount(recipient) ..
    " unread messages now.")

    return true
end

local function save_draft(player, recipient, topic, message)

    local name = player:get_player_name()

    local temprecipient = recipient
    local temptopic = topic
    local tempmessage = message

    if not temprecipient or temprecipient == "" then
        temprecipient = "No Recipient"
    end

    if not temptopic or temptopic == "" then
        temptopic = "No Topic"
    end

    table.insert(mail[name].drafts,
    {
        to = temprecipient,
        topic = temptopic,
        content = tempmessage
    })

    user_message(name, "Saved current message as draft.")
    update_formspec(player)

end

-------------------------------------------------------------------------------
--Formspec actions
-------------------------------------------------------------------------------

local function formspec_on_player_receive_fields(self, player, context, fields)

    local name = player:get_player_name()
    local recipient = fields[mod_name .. "_to_from_field"]
    local topic = fields[mod_name .. "_topic_field"]
    local message = fields[mod_name .. "_message_area"]

    ---------------------------------------------------------------------------
    --Inbox table selecting
    ---------------------------------------------------------------------------

    if fields[mod_name .. "_inbox_table"] then

        local event = minetest.explode_table_event(
        fields[mod_name .. "_inbox_table"])

        if event.type == "INV" then
            player_context[name].inbox_id = nil
        else
            if event.row == 1 then return end
            player_context[name].inbox_id = event.row - 1
            if player_context[name].compositing then
                player_context[name].compositing = nil
            end
        end

        update_formspec(player)

    ---------------------------------------------------------------------------
    --Drafts table selecting
    ---------------------------------------------------------------------------

     elseif fields[mod_name .. "_drafts_table"] then

        local event = minetest.explode_table_event(
        fields[mod_name .. "_drafts_table"])

        if event.type == "INV" then
            player_context[name].draft_id = nil
        else
            if event.row == 1 then return end
            player_context[name].draft_id = event.row - 1
            if not player_context[name].compositing then
                player_context[name].compositing = true
            end
        end

        update_formspec(player)

    ---------------------------------------------------------------------------
    --Send Mail
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_send_button"] then

         send_mail(player, recipient, topic, message)

    ---------------------------------------------------------------------------
    --Delete mail
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_inbox_delete_button"] then

        if not player_context[name].inbox_id then return end
        if not mail[name].inbox[player_context[name].inbox_id] then return end

        table.remove(mail[name].inbox, player_context[name].inbox_id)

        if player_context[name].inbox_id > #mail[name].inbox and
        player_context[name].inbox_id > 1 then

            player_context[name].inbox_id = player_context[name].inbox_id - 1

        end

        update_formspec(player)

    ---------------------------------------------------------------------------
    --Delete draft
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_draft_delete_button"] then

        if not player_context[name].draft_id then return end
        if not mail[name].drafts[player_context[name].draft_id] then return end

        table.remove(mail[name].drafts, player_context[name].draft_id)

        if player_context[name].draft_id > #mail[name].drafts and
        player_context[name].draft_id > 1 then

            player_context[name].draft_id = player_context[name].draft_id - 1

        end

        update_formspec(player)

    ---------------------------------------------------------------------------
    --Save Draft
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_save_draft"] then

         save_draft(player, recipient, topic, message)

    ---------------------------------------------------------------------------
    --New Mail
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_new_mail"] then

         if not player_context[name].compositing then
             player_context[name].compositing = true
         end

         player_context[name].clear = true

         update_formspec(player)
     end
end

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

sfinv.register_page(mod_name .. ":mail",
    {
        title = "Mail",
        get = get_mail_formspec,
        on_player_receive_fields = formspec_on_player_receive_fields,
    }
)

minetest.register_on_joinplayer(function(player)

    local name = player:get_player_name()

    if not mail[name] then
        mail[name] = {}
        mail[name].inbox = {}
        mail[name].drafts = {}
    end

    player_context[name] = {}

    local unread = get_unread_amount(name)

    if unread > 0 then
        user_message(name, "You have " .. unread .. " new messages!")
    end

end)

minetest.register_on_leaveplayer(function(player)
    local name = player:get_player_name()
    player_context[name] = nil
end)

if require_paper then

    minetest.register_craftitem(mod_name .. ":envelope", {
        description = "Envelope",
        inventory_image = mod_name .. "_envelope.png",
    })

    minetest.register_craft({
        output = mod_name .. ":envelope",
        recipe = {
            {"", "", ""},
            {"", "default:paper", ""},
            {"", "default:paper", ""},
        },
    })

end
