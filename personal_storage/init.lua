-------------------------------------------------------------------------------
--Settingtype initialization
-------------------------------------------------------------------------------

local mod_name = "personal_storage"

local function get_setting(name, def)

    if type(def) == "boolean" then
        local inp = minetest.settings:get_bool(mod_name .. "_" .. name, def)
        return inp
    elseif type(def) == "string" then
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return inp
    else
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return tonumber(inp)
    end

end

local inventory_size = get_setting("inventory_size", 32)

minetest.register_node(mod_name .. ":personal_chest", {
    --TODO: add other fields
    description = "Personal Chest",
    paramtype = "light",

    on_construct = function(pos)

        local meta = minetest.get_meta(pos)
        meta:set_string("infotext", "Personal Chest")

    end,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)

        if not clicker then return end

        local name = clicker:get_player_name()

        if not name then return end

        local formspec = {}

        formspec[#formspec + 1] = "size[8,10.25]"
        formspec[#formspec + 1] = default.gui_bg
        formspec[#formspec + 1] = default.gui_bg_img
        formspec[#formspec + 1] = default.gui_slots
        formspec[#formspec + 1] = "list[current_player;"
        formspec[#formspec + 1] = mod_name
        formspec[#formspec + 1] = "_personal"
        formspec[#formspec + 1] = ";0,0;8,6;]"
        formspec[#formspec + 1] = "list[current_player;main;0,6.5;8,4;]"

        minetest.show_formspec(name,
        mod_name .. "_personal_chest", table.concat(formspec))

    end,
})

minetest.register_on_joinplayer(function(player)
    if not player then return end
    local inv = player:get_inventory()
    if not inv then return end
    if inv:get_size(mod_name .. "_personal") == 0 then
        inv:set_size(mod_name .. "_personal", inventory_size)
    end
end)
