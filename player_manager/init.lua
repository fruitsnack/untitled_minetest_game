-------------------------------------------------------------------------------
--Default tables
-------------------------------------------------------------------------------

local player_list = {}
local player_context = {}

-------------------------------------------------------------------------------
--Settingtypes
-------------------------------------------------------------------------------

local mod_name = "player_manager"

local function get_setting(name, def, bool)

    if bool then
        local inp = minetest.settings:get_bool(mod_name .. "_" .. name, def)
        return inp
    else
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return tonumber(inp)
    end

end

local update_interval = get_setting("update_interval", 1)

-------------------------------------------------------------------------------
--Internal variables
-------------------------------------------------------------------------------

local max_users = minetest.settings:get("max_users") or 15
local skinsdb_enabled = false
local simple_parties_enabled = false

local name_colors = {"#2fb6c8", "#29be9a"}
local latency_colors = {"#e8a32d", "#bf6736"}
local playtime_colors = {"#90d685", "#8aa25e"}
local address_colors = {"#b087cc", "#714094"}
local party_color = "#1fdb8c"

-------------------------------------------------------------------------------
--Mod Integrations
-------------------------------------------------------------------------------

if minetest.get_modpath("skinsdb") then
    skinsdb_enabled = true
end

if minetest.get_modpath("parties") then
    simple_parties_enabled = true
end

-------------------------------------------------------------------------------
--Utilities
-------------------------------------------------------------------------------

local function build_skinrows(previews)
    local result = {}
    for k,v in pairs(previews) do
        result[#result + 1] = k
        result[#result + 1] = "="
        result[#result + 1] = v
        result[#result + 1] = ","
    end
    result[#result] = nil
    return table.concat(result)
end

local function build_formspec(caller)

    local player_names = {}
    local previews = {}

    local privs = minetest.get_player_privs(caller:get_player_name())

    if skinsdb_enabled then
        player_names[#player_names + 1] = ""
    end

    player_names[#player_names + 1] = name_colors[1]
    player_names[#player_names + 1] = "Name"

    if simple_parties_enabled then
        player_names[#player_names + 1] = party_color
        player_names[#player_names + 1] = "Party"
    end

    player_names[#player_names + 1] = latency_colors[1]
    player_names[#player_names + 1] = "Latency"
    player_names[#player_names + 1] = playtime_colors[1]
    player_names[#player_names + 1] = "Online for"

    if privs.ipview then
        player_names[#player_names + 1] = address_colors[1]
        player_names[#player_names + 1] = "Address"
    end

    for _,v in pairs(player_list) do

        local name = v.ref:get_player_name()
        local info = minetest.get_player_information(name)
        local latency = math.ceil((info.avg_rtt / 2) * 1000)
        local hours = math.floor(info.connection_uptime / 60 / 60)
        local minutes = math.floor((info.connection_uptime / 60) % 60)

        if skinsdb_enabled then

            local texture = skins.get_player_skin(v.ref):get_preview()
            local index = #previews + 1
            local transform = "(" .. texture .. ")^[resize:32x64"
            previews[index] = minetest.formspec_escape(transform)
            player_names[#player_names + 1] = index

        end

        player_names[#player_names + 1] = name_colors[2]
        player_names[#player_names + 1] = name

        if simple_parties_enabled then

            local party = parties.get_player_party(name)

            if party then

                local col = parties.get_party_info(party).name_color

                player_names[#player_names + 1] = minetest.rgba(col.r, col.g, col.b)
                player_names[#player_names + 1] = party

            else

                player_names[#player_names + 1] = ""
                player_names[#player_names + 1] = ""

            end

        end

        player_names[#player_names + 1] = latency_colors[2]
        player_names[#player_names + 1] = latency .. " ms"
        player_names[#player_names + 1] = playtime_colors[2]
        player_names[#player_names + 1] = hours .. "H" .. minutes .. "M"

        if privs.ipview then
            player_names[#player_names + 1] = address_colors[2]
            player_names[#player_names + 1] = info.address
        end

    end

    local formspec = {}

    formspec[#formspec + 1] = "label[0,-0.22;Players: "
    formspec[#formspec + 1] = #player_list
    formspec[#formspec + 1] = "/"
    formspec[#formspec + 1] = max_users
    formspec[#formspec + 1] = "]"
    formspec[#formspec + 1] = "tableoptions[border=true,highlight=#00000000]"
    formspec[#formspec + 1] = "tablecolumns["

    if skinsdb_enabled then
        formspec[#formspec + 1] = "image,align=center,width=3.5,padding=1,"
        formspec[#formspec + 1] = build_skinrows(previews)
        formspec[#formspec + 1] = ";"
    end

    formspec[#formspec + 1] = "color,span=1;"
    formspec[#formspec + 1] = "text,align=left,width=10,padding=1;"

    if simple_parties_enabled then
        formspec[#formspec + 1] = "color,span=1;"
        formspec[#formspec + 1] = "text,align=left,width=10,padding=1;"
    end

    formspec[#formspec + 1] = "color,span=1;"
    formspec[#formspec + 1] = "text,align=right,width=7,padding=1;"

    if privs.ipview then
        formspec[#formspec + 1] = "color,span=1;"
        formspec[#formspec + 1] = "text,align=center,width=8,padding=1;"
        formspec[#formspec + 1] = "color,span=1;"
        formspec[#formspec + 1] = "text,align=right,width=12,padding=1]"
    else
        formspec[#formspec + 1] = "color,span=1;"
        formspec[#formspec + 1] = "text,align=center,width=8,padding=1]"
    end

    formspec[#formspec + 1] = "table[0,0.3;7.8,4.75;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_table;"
    formspec[#formspec + 1] = table.concat(player_names, ",")
    formspec[#formspec + 1] = "]"

    return table.concat(formspec)
end

local function get_playerlist_formspec(self, player, context)
    return sfinv.make_formspec(
    player,
    context,
    build_formspec(player),
    false,
    "size[8,5]")
end

local function on_player_receive_fields(self, player, context, fields)
    return
end

local function formspec_on_enter(self, player, context)
    local name = player:get_player_name()
    player_context[name] = {ref = player}
end

local function formspec_on_leave(self, player, context)
    local name = player:get_player_name()
    player_context[name] = nil
end

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

sfinv.register_page(mod_name .. ":player_manager",
    {
        title = "Players",
        get = get_playerlist_formspec,
        on_player_receive_fields = on_player_receive_fields,
        on_enter = formspec_on_enter,
        on_leave = formspec_on_leave,
    }
)

local timer = 0
minetest.register_globalstep(function(dtime)

        timer = timer + dtime
        if timer < update_interval then return end

        for k,v in pairs(player_context) do
            sfinv.set_page(v.ref, mod_name .. ":player_manager")
        end

        timer = 0
end)

minetest.register_privilege("ipview", {
    description = "Allows to see user IPs in player manager",
    give_to_singleplayer = false,
    give_to_admin = true,
})

minetest.register_on_joinplayer(function(player)

        local name = player:get_player_name()
        table.insert(player_list, {name = name, ref = player})

end)

minetest.register_on_leaveplayer(function(player)

    local name = player:get_player_name()

    for k,v in pairs(player_list) do
        if v.name == name then
            table.remove(player_list, k)
            break
        end
    end

    if player_context[name] then
        player_context[name] = nil
    end

end)
