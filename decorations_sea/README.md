# decorations_sea
This mod adds various types of sea decorations spread across biomes.

![Screenshot](screenshot.png)

## Requirements

- Minetest 5.0.0+
- Minetest_game 5.0.0+

## Recommended mods

- [lootchests_modpack](https://github.com/ClockGen/lootchests_modpack), Adds rare treasure chests and barrels to the ocean
- [shipwrecks](https://github.com/ClockGen/shipwrecks), Adds configurable and lootable shipwrecks

## License
Kelp on_place() code is copied from minetest_game, copyright Various Minetest developers and contributors (LGPLv2.1+)
All other code is licensed under AGPL-3.0-only [link to the license](https://spdx.org/licenses/AGPL-3.0-only.html)
All resources are licensed under CC 4.0 Attribution-NonCommercial-NoDerivs 4.0 International (CC BY-NC-ND 4.0) [link to the license](https://creativecommons.org/licenses/by-nc-nd/4.0/)


