-------------------------------------------------------------------------------
--Default Data Structures
-------------------------------------------------------------------------------

emoji = {}
local emoji_list = {}

-------------------------------------------------------------------------------
--Settingtypes
-------------------------------------------------------------------------------

local mod_name = "emoji"

local function get_setting(name, def, bool)

    if bool then
        local inp = minetest.settings:get_bool(mod_name .. "_" .. name, def)
        return inp
    else
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return tonumber(inp)
    end

end

local emoji_sound_gain = get_setting("sound_gain", 1)
local emoji_duration = get_setting("duration", 4)
local emoji_size = get_setting("size", 6)
local emoji_glow = get_setting("glow", true, false)

-------------------------------------------------------------------------------
--Internal variables
-------------------------------------------------------------------------------

local total_emojis = 0
local emojis_per_row = 0
local emoji_light_level = 0
local emoji_formspec = ""

if emoji_glow then
    emoji_light_level = 14
end

-------------------------------------------------------------------------------
--Utilities
-------------------------------------------------------------------------------

local function msg(level, input)
    minetest.log(level, "[" .. mod_name .. "] " .. input)
end

local function build_emoji_list()

    local counter = 0
    local scale_factor = (4/emojis_per_row)*2
    local missing_rows = math.floor(((emojis_per_row * emojis_per_row) -
    total_emojis) / emojis_per_row)
    local ydisp = (scale_factor / 2) * missing_rows

    local formspec = {}

    for k,v in pairs(emoji_list) do

        local ypos = math.floor(counter / emojis_per_row)
        local xpos = counter % emojis_per_row

        local element = {}

        element[#element + 1] = "image_button["
        element[#element + 1] = xpos * scale_factor
        element[#element + 1] = ","
        element[#element + 1] = (ypos * scale_factor) + 0.1 + ydisp
        element[#element + 1] = ";"
        element[#element + 1] = scale_factor
        element[#element + 1] = ","
        element[#element + 1] = scale_factor
        element[#element + 1] = ";"
        element[#element + 1] = v.image
        element[#element + 1] = ";"
        element[#element + 1] = k
        element[#element + 1] = ";]"

        formspec[#formspec + 1] = table.concat(element)

        counter = counter + 1
    end

    emoji_formspec = table.concat(formspec)
end

local function formspec_action(self, player, context, fields)

    local pos = player:get_pos()

    for k,_ in pairs(fields) do

        if k == "quit" then return end

        local emojidef = emoji_list[k]

        minetest.add_particle({
            pos = {x=pos.x, y=pos.y+2, z=pos.z},
            velocity = {x=0, y=0.5, z=0},
            expirationtime = emoji_duration,
            size = emoji_size,
            glow = emoji_light_level,
            texture = emojidef.image,
        })

        minetest.sound_play(emojidef.sound,
        {
            pos=pos,
            gain=emoji_sound_gain,
            max_hear_distance=2*64
        },
        true)

        break

    end
end

local function get_emoji_list(self, player, context)
    return sfinv.make_formspec(
    player,
    context,
    emoji_formspec,
    false,
    "size[8,8]")
end

local function process_emojis()
    for _,_ in pairs(emoji_list) do
        total_emojis = total_emojis + 1
    end
    emojis_per_row = math.ceil(math.sqrt(total_emojis))
    build_emoji_list()
end

-------------------------------------------------------------------------------
--API
-------------------------------------------------------------------------------

emoji.register_emoji = function(name, image, sound)
    if emoji_list[name] == nil then
        emoji_list[name] = {image = image, sound = sound}
    else
        msg("error", "Emoji " .. name .. " is already defined!")
    end
end

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

minetest.register_on_mods_loaded(process_emojis)

sfinv.register_page(mod_name .. ":emojis",
    {
        title = "Emojis",
        get = get_emoji_list,
        on_player_receive_fields = formspec_action,
    }
)
