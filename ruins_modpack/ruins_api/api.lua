local function generate_room()
    return
end


minetest.register_on_generated(function(minp, maxp, seed)

    local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
    local area = VoxelArea:new({MinEdge = emin, MaxEdge = emax})
    local data = vm:get_data()
    local sidelength = maxp.x - minp.x + 1
    print(sidelength)

    local c_stone = minetest.get_content_id("default:stone")
    local c_torch = minetest.get_content_id("default:torch")
    local c_air = minetest.get_content_id("air")

    local center = {
        x = minp.x + sidelength/2,
        y = minp.y + sidelength/2,
        z = minp.z + sidelength/2
    }

    for i = asd.x,maxp.x,1 do
        for j = 1,5,1 do
            for k = minp.z,maxp.z,1 do
                local vi = area:index(i, center.y + j, k)
                if data[vi] == c_stone then
                    data[vi] = c_torch
                end
            end
        end
     end

    vm:set_data(data)
    vm:calc_lighting()
    vm:write_to_map(true)
    vm:update_liquids()

end)
