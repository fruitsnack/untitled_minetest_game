local path = minetest.get_modpath("decorations_ruins")

local noise_def = {
    offset = -1.3925,
    scale = 1,
    spread = {x = 64, y = 64, z = 64},
    seed = 1121,
    octaves = 3,
    persist = 0.5,
    lacunarity = 2,
}
--[[
local noise_def = {
    offset = -1.1,
    scale = 1,
    spread = {x = 64, y = 64, z = 64},
    seed = 1121,
    octaves = 5,
    persist = 0.5,
    lacunarity = 2,
}]]


for _,v in pairs({"oak", "jungle", "pine"}) do
    for i = 1, 6 do
        minetest.register_decoration({
            deco_type = "schematic",
            place_on = {"default:sand", "default:silver_sand", "default:desert_sand"},
            sidelen = 16,
            --fill_ratio = 0.0000001,
            noise_params = noise_def,
            --biomes = {"Oceanside", "Hills", "Plains"},
            y_min = -128,
            y_max = -3,
            spawn_by = "default:water_source",
            num_spawn_by = 8,
            flags = "force_placement, all_floors",
            schematic = path .. "/schematics/decorations_sea_shipwreck_" .. v .. "_0" .. i .. ".mts",
            rotation = "random",
            place_offset_y = -1,
        })
    end
end