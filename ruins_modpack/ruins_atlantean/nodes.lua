minetest.register_node("decorations_ruins:atlantean_brick", {
    description = "Atlantean Brick",
    tiles = {
        "decorations_ruins_atlantean_brick.png",
    },
    is_ground_content = false,
    groups = {cracky = 1, level = 2},
    sounds = default.node_sound_stone_defaults(),
})

stairs.register_stair_and_slab(
    "atlantean_brick",
    "decorations_ruins:atlantean_brick",
    {cracky=1, level=2},
    {"decorations_ruins_atlantean_brick.png"},
    "Atlantean Brick Stair",
    "Atlantean Brick Slab",
    default.node_sound_stone_defaults(),
    false
)

minetest.register_node("decorations_ruins:atlantean_column_01", {
    description = "Atlantean Column",
    tiles = {
        "decorations_ruins_atlantean_middle_01.png",
        "decorations_ruins_atlantean_middle_01.png",
        "decorations_ruins_atlantean_column_01.png",
    },
    is_ground_content = false,
    groups = {cracky = 1, level = 2},
    sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("decorations_ruins:atlantean_column_02", {
    description = "Atlantean Column",
    tiles = {
        "decorations_ruins_atlantean_middle_01.png",
        "decorations_ruins_atlantean_middle_01.png",
        "decorations_ruins_atlantean_column_02.png",
    },
    is_ground_content = false,
    groups = {cracky = 1, level = 2},
    sounds = default.node_sound_stone_defaults(),
})

for i = 1, 3 do
    minetest.register_node("decorations_ruins:atlantean_wall_01", {
        description = "Atlantean Wall",
        tiles = {
            "decorations_ruins_atlantean_middle_0" .. i .. ".png",
        },
        is_ground_content = false,
        groups = {cracky = 1, level = 2},
        sounds = default.node_sound_stone_defaults(),
    })

    stairs.register_stair_and_slab(
        "atlantean_wall_0" .. i,
        "decorations_ruins:atlantean_wall_0" .. i,
        {cracky=1, level=2},
        {"decorations_ruins_atlantean_middle_0" .. i .. ".png"},
        "Atlantean Wall Stair",
        "Atlantean Wall Slab",
        default.node_sound_stone_defaults(),
        false
    )
end