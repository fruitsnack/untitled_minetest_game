# Untitled minetest gamemode
## Description

This is the source code and assets from an untitled minetest gamemode that I've been developing somewhere in 2017-2021.  
Unfortunately, due to lack of time, interest and motivation, this project was eventually abandoned, and I decided to put it up as it is.  

Currently, **each mod subdirectory contains ancient READMEs and obsolete links**, refer to this document as the newest one.  

This project was also left in a very messy state, with lots of debug and test code that was meant to be eventually removed.  
Assets structure is also messy and contains both old and new assets unsorted.  

This gamemode was meant to be built up on top of minetest_game, with gameplay focus on underground exploration and crafting of either magic or tech trees.
It also included many popular minetest mods and frameworks to build up on top of them and not reinvent the wheel (mesecons, playereffects, mobs_redo, etc),
however I decided not to include them, (even as git submodules), because this gamemode is neither complete, and the focus of this repository is preservation
of additions made in this gamemode only.

## Project structure
At some point, every mod in this game was located in a separate repo and added as git submodule, with main repo being rebased off minetest_game and contained
project-specific patches. This is not the case anymore and this repo is just a pure storage of parts of gamemode that was in development.

This is not a proper minetest game structure and wouldn't work as it is.  
`Assets` directory contains project files for all assets made for this gamemode.  

## Will it work now?
Currently, last code was commited somewhere in 2021 and by now even functional parts are hopelessly outdated, so it's very unlikely that any part of this
will work in current minetest version.

## Completion status
Codewise, many parts were already implemented or in process of rewrite.  

For most mods in this gamemode, assets were complete and can be found in `Assets` directory. They, however, were not updated in mod directories themselves and most mods contain
outdated assets that were remade or improved later.  

## License
Generally, unless stated otherwise specifically in respective directories:  
All code is made by me (Fruitsnack) and licensed under AGPL-3.0-only [link to the license](https://spdx.org/licenses/AGPL-3.0-only.html).  
All resources are made by me (Fruitsnack) and licensed under CC 4.0 Attribution-NonCommercial-NoDerivs 4.0 International (CC BY-NC-ND 4.0) [link to the license](https://creativecommons.org/licenses/by-nc-nd/4.0/).  
