-------------------------------------------------------------------------------
--Settingtype initialization
-------------------------------------------------------------------------------

local mod_name = "technic_transporter"

local function get_setting(name, def)

    if type(def) == "boolean" then
        local inp = minetest.settings:get_bool(mod_name .. "_" .. name, def)
        return inp
    elseif type(def) == "string" then
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return inp
    else
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return tonumber(inp)
    end

end

local cost_multiplier = get_setting("cost_multiplier", 20)
local bidirectional_multiplier = get_setting("bidirectional_multiplier", 2.5)
local bookmarks_max_length = get_setting("bookmarks_max_length", 16)

-------------------------------------------------------------------------------
--Internal variables
-------------------------------------------------------------------------------

local node_name = mod_name .. ":transporter"
local node_name_active = mod_name .. ":transporter_active"

local world_limits = {
    min_x = -31000, max_x = 31000,
    min_y = -31000, max_y = 31000,
    min_z = -31000, max_z = 31000
}

-------------------------------------------------------------------------------
--Formspec building
-------------------------------------------------------------------------------

local function build_formspec(bookmarks, destination, enabled, cost,
    bidirectional, one_time)

    local formspec = {}

    formspec[#formspec + 1] = "size[6,4]"
    formspec[#formspec + 1] = default.gui_bg
    formspec[#formspec + 1] = default.gui_bg_img
    formspec[#formspec + 1] = "box[-0.09,-0.12;2.975,1.375;#00000120]"
    formspec[#formspec + 1] = "box[-0.09,1.335;2.975,2.91;#00000120]"
    formspec[#formspec + 1] = "box[2.95,-0.12;2.975,4.375;#00000120]"

    formspec[#formspec + 1] = "textarea[0.27,-0.12;2,1;;Information:;]"

    formspec[#formspec + 1] = "textarea[0.27,0.2;4,2;;Target: X:"
    formspec[#formspec + 1] = destination.x
    formspec[#formspec + 1] = " Y:"
    formspec[#formspec + 1] = destination.y
    formspec[#formspec + 1] = " Z:"
    formspec[#formspec + 1] = destination.z
    formspec[#formspec + 1] = ";]"

    formspec[#formspec + 1] = "textarea[0.27,0.45;28,1;;Cost per second:"
    formspec[#formspec + 1] = cost
    formspec[#formspec + 1] = "EU;]"

    formspec[#formspec + 1] = "textarea[0.27,0.7;28,1;;Bidirectional: "
    formspec[#formspec + 1] = tostring(bidirectional)
    formspec[#formspec + 1] = ";]"

    formspec[#formspec + 1] = "textarea[0.27,0.95;28,1;;One time: "
    formspec[#formspec + 1] = tostring(one_time)
    formspec[#formspec + 1] = ";]"

    formspec[#formspec + 1] = "checkbox[-0.02,2;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_bidirectional;Bidirectional;"
    formspec[#formspec + 1] = tostring(bidirectional)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "checkbox[1.3,2;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_one_time;One Time;"
    formspec[#formspec + 1] = tostring(one_time)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "textarea[3.325,-0.12;2,1;;Bookmarks:;]"

    formspec[#formspec + 1] = "field[0.28,1.825;1,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_x;X;"
    formspec[#formspec + 1] = destination.x
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "field[1.28,1.825;1,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_y;Y;"
    formspec[#formspec + 1] = destination.y
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "field[2.28,1.825;1,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_z;Z;"
    formspec[#formspec + 1] = destination.z
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "field[3.295,2.9;3.05,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_bookmark_name;Save position;]"

    formspec[#formspec + 1] = "button[3,3.4;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_bookmark_save;Save]"

    formspec[#formspec + 1] = "button[0.75,2.575;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_set_destination;Set]"

    formspec[#formspec + 1] = "button[4.565,3.4;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_bookmark_delete;Delete]"

    formspec[#formspec + 1] = "tablecolumns["
    formspec[#formspec + 1] = "text,align=left,width=5,padding=0.5;"
    formspec[#formspec + 1] = "text,align=left,width=3,padding=0.5;"
    formspec[#formspec + 1] = "text,align=left,width=3,padding=0.5;"
    formspec[#formspec + 1] = "text,align=left,width=3,padding=0.5]"

    formspec[#formspec + 1] = "table[3,0.13;2.85,2.25;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_bookmarks;"
    formspec[#formspec + 1] = bookmarks
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "button_exit[0.5,3.4;2,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_accept;"
    formspec[#formspec + 1] = enabled
    formspec[#formspec + 1] = "]"

    return table.concat(formspec)
end

-------------------------------------------------------------------------------
--Transporter effects
-------------------------------------------------------------------------------
--TODO: dehardcode asset names in effects
local function transporter_entrance_effect(pos)
    minetest.add_particlespawner({
        amount = 32,
        time = 4,
        minpos = {x=pos.x-0.5, y=pos.y-0.5, z=pos.z-0.5},
        maxpos = {x=pos.x+0.5, y=pos.y-0.5, z=pos.z+0.5},
        minvel = {x=0, y=1, z=0},
        maxvel = {x=0, y=4, z=0},
        minacc = {x=0, y=0.1, z=0},
        maxacc = {x=0, y=0.5, z=0},
        minexptime = 1,
        maxexptime = 1,
        minsize = 3,
        maxsize = 4,
        collisiondetection = false,
        collision_removal = false,
        glow = 14,
        vertical = true,
        texture = "transporter_particle_idle.png",
     })
end

local function transporter_exit_effect(pos)
    minetest.add_particlespawner({
        amount = 32,
        time = 4,
        minpos = {x=pos.x-0.5, y=pos.y+5, z=pos.z-0.5},
        maxpos = {x=pos.x+0.5, y=pos.y+4, z=pos.z+0.5},
        minvel = {x=0, y=-1, z=0},
        maxvel = {x=0, y=-4, z=0},
        minacc = {x=0, y=-0.1, z=0},
        maxacc = {x=0, y=-0.5, z=0},
        minexptime = 1,
        maxexptime = 1,
        minsize = 3,
        maxsize = 4,
        collisiondetection = false,
        collision_removal = false,
        glow = 14,
        vertical = true,
        texture = "transporter_particle_idle.png",
     })
end

local function transporter_on_teleport_effect(pos)

    minetest.sound_play("transporter_transport", {
        pos = pos,
        gain = 1,
        max_hear_distance=16
    })

    minetest.add_particlespawner({
        amount = 64,
        time = 0.5,
        minpos = {x=pos.x-0.5, y=pos.y, z=pos.z-0.5},
        maxpos = {x=pos.x+0.5, y=pos.y+2, z=pos.z+0.5},
        minvel = {x=-5, y=-2, z=-5},
        maxvel = {x=5, y=2, z=5},
        minacc = {x=0, y=9.8, z=0},
        maxacc = {x=0, y=9.8, z=0},
        minexptime = 1,
        maxexptime = 3,
        minsize = 3,
        maxsize = 8,
        collisiondetection = false,
        collision_removal = false,
        glow = 14,
        vertical = true,
        texture = "transporter_particle_idle.png",
     })

end

-------------------------------------------------------------------------------
--Metadata utils
-------------------------------------------------------------------------------

local function msg(level, input)
    minetest.log(level, "[" .. mod_name .. "] " .. input)
end

local function fastpow(a, b)
    local state = a
    for i = 1, b - 1 do
        state = state * a
    end
    return state
end

local function distance(pos1, pos2)
    return math.sqrt(
    fastpow(pos1.x - pos2.x, 2) +
    fastpow(pos1.y - pos2.y, 2) +
    fastpow(pos1.z - pos2.z, 2)
    )
end

local function user_message(name, message)

    local message = minetest.colorize(message_color,
    "<" .. mod_name .. "> " .. message)

    minetest.chat_send_player(name, message)

end

local function update_node_formspec(meta)

    local bookmarks = minetest.deserialize(meta:get_string("bookmarks"))
    local destination = minetest.deserialize(meta:get_string("destination"))
    local cost = tonumber(meta:get_int("cost"))
    local bidirectional = meta:get_string("bidirectional")
    local one_time = meta:get_string("one_time")
    local enabled = "Energize!"

    if minetest.is_yes(meta:get_string("enabled")) then enabled = "Disable" end

    local bookmark_list = {}
    for k,v in pairs(bookmarks) do
        bookmark_list[#bookmark_list + 1] = v.name
        bookmark_list[#bookmark_list + 1] = v.x
        bookmark_list[#bookmark_list + 1] = v.y
        bookmark_list[#bookmark_list + 1] = v.z
    end

    meta:set_string("formspec",
        build_formspec(
            table.concat(bookmark_list, ","),
            destination,
            enabled,
            cost,
            bidirectional,
            one_time
        )
    )

end

local function calculate_cost(pos, meta)

    local destination = minetest.deserialize(meta:get_string("destination"))

    local temp_destination = {
        x = tonumber(destination.x),
        y = tonumber(destination.y),
        z = tonumber(destination.z)
    }

    local cost = math.ceil(distance(pos, temp_destination) * cost_multiplier)

    if minetest.is_yes(meta:get_string("bidirectional")) then
        cost = cost * bidirectional_multiplier
    end

    return cost
end

local function toggle_transporter(pos, state)
    if state then
        minetest.swap_node(pos, {name=node_name_active})
    else
        minetest.swap_node(pos, {name=node_name})
    end
end

local function test_fits_world(fields, sender)

    if tonumber(fields[mod_name .. "_x"]) < world_limits.min_x or
    tonumber(fields[mod_name .. "_x"]) > world_limits.max_x or
    tonumber(fields[mod_name .. "_y"]) < world_limits.min_y or
    tonumber(fields[mod_name .. "_y"]) > world_limits.max_y or
    tonumber(fields[mod_name .. "_z"]) < world_limits.min_z or
    tonumber(fields[mod_name .. "_z"]) > world_limits.max_z then
        user_message(sender, "Coordinates can't go beyond world borders!")
        return false
    end

    return true
end

local function test_numeric(fields, sender)

    if not tonumber(fields[mod_name .. "_x"]) or
    not tonumber(fields[mod_name .. "_y"]) or
    not tonumber(fields[mod_name .. "_z"]) then
        user_message(sender, "Invalid coordinates!")
        return false
    end

    return true
end

local function transport_objects(from, to, meta)

    local objs = minetest.get_objects_inside_radius(from, 1.5)

    for k,v in pairs(objs) do
        if v:is_player() and
        v:get_player_name() ~= meta:get_string("last_user") then

            v:set_pos(to)
            transporter_on_teleport_effect(to)
            meta:set_string("last_user", v:get_player_name())

            if minetest.is_yes(meta:get_string("one_time")) then
                meta:set_int("enabled", 0)
                meta:set_int("HV_EU_demand", 0)
            end

        else

            if meta:get_string("last_user") ~= "" then
                meta:set_string("last_user", "")
            end

        end
    end

end

--TODO:add all used metadata fields in initialization
local function transporter_on_construct(pos)

    local meta = minetest.get_meta(pos)

    meta:set_string("bookmarks",
        minetest.serialize({{name = "Name", x = "X", y = "Y", z = "Z"}}))
    meta:set_string("destination", minetest.serialize({x = 0, y = 0, z = 0}))
    meta:set_string("enabled", "false")
    meta:set_string("bidirectional", "false")
    meta:set_string("one_time", "false")
    meta:set_int("cost", calculate_cost(pos, meta))
    meta:set_string("infotext", "HV Transporter disabled")
    meta:set_int("HV_EU_demand", 0)

    update_node_formspec(meta)
end

-------------------------------------------------------------------------------
--Formspec processing
-------------------------------------------------------------------------------

local function transporter_on_receive_fields(pos, form_name, fields, sender)

    local meta = minetest.get_meta(pos)
    local enabled = minetest.is_yes(meta:get_string("enabled"))

    if fields[mod_name .. "_accept"] and not enabled then

        meta:set_string("enabled", "true")
        meta:set_int("HV_EU_demand", meta:get_int("cost"))
        update_node_formspec(meta)

    elseif fields[mod_name .. "_accept"] and enabled then

        meta:set_string("enabled", "false")
        meta:set_int("HV_EU_demand", 0)
        update_node_formspec(meta)

    elseif fields[mod_name .. "_bookmark_save"] then

        if not test_numeric(fields, sender) then return end
        if not test_fits_world(fields, sender) then return end

        if fields.name == "" then
            user_message(sender, "Bookmark name can't be empty!")
            return
        end

        if string.len(fields[mod_name .. "_bookmark_name"]) >
        bookmarks_max_length then
            user_message(sender,
                "Bookmark name should be shorter than " ..
                tostring(bookmarks_max_length) .. " characters!")
            return
        end

        local temp_bookmark = {
            x = math.floor(fields[mod_name .. "_x"]),
            y = math.floor(fields[mod_name .. "_y"]),
            z = math.floor(fields[mod_name .. "_z"]),
            name = fields.name
        }

        local temp_metadata = {}

        if meta:get_string("bookmarks") ~= "" then 
            temp_metadata = minetest.deserialize(meta:get_string("bookmarks"))
        end

        table.insert(temp_metadata, temp_bookmark)
        meta:set_string("bookmarks", minetest.serialize(temp_metadata))
        update_node_formspec(meta)

    elseif fields[mod_name .. "_set_destination"] then

        if not test_numeric(fields, sender) then return end
        if not test_fits_world(fields, sender) then return end

        meta:set_string("destination",
            minetest.serialize({
                x = tonumber(fields[mod_name .. "_x"]),
                y = tonumber(fields[mod_name .. "_y"]),
                z = tonumber(fields[mod_name .. "_z"])
            })
        )

        meta:set_int("cost", calculate_cost(pos, meta))
        update_node_formspec(meta)

    elseif fields[mod_name .. "_bookmark_delete"] then

    local field = meta:get_int("table_field")

    if field ~= 1 then

        local temp_metadata = minetest.deserialize(
            meta:get_string("bookmarks"))

        table.remove(temp_metadata, field)
        meta:set_string("bookmarks", minetest.serialize(temp_metadata))

        if field > #temp_metadata then
            meta:set_int("table_field", field - 1)
        end

        update_node_formspec(meta)

    end

    elseif fields[mod_name .. "_bookmarks"] then

        local table_field = tonumber(
            string.match(fields[mod_name .. "_bookmarks"], '%d+'))

        if table_field ~= 1 then

            local temp_metadata = minetest.deserialize(
                meta:get_string("bookmarks"))

            meta:set_string("destination",
                minetest.serialize(temp_metadata[table_field]))
            meta:set_int("cost", calculate_cost(pos, meta))
            meta:set_int("tablefield", table_field)

            update_node_formspec(meta)

        end

    elseif fields[mod_name .. "_one_time"] then

        meta:set_string("onetime", fields[mod_name .. "_one_time"])
        update_node_formspec(meta)

    elseif fields[mod_name .. "_bidirectional"] then

        meta:set_string("bidirectional", fields[mod_name .. "_bidirectional"])
        meta:set_int("cost", calculate_cost(pos, meta))
        update_node_formspec(meta)

    end
end

-------------------------------------------------------------------------------
--Technic integration
-------------------------------------------------------------------------------

local function transporter_on_technic_run(pos, node)

    local meta = minetest.get_meta(pos)
    local eu_input = meta:get_int("HV_EU_input")
    local demand = meta:get_int("HV_EU_demand")
    local enabled = minetest.is_yes(meta:get_string("enabled"))
    local destination = minetest.deserialize(meta:get_string("destination"))

    if enabled and eu_input >= demand then

        if node.name == node_name then
            toggle_transporter(pos, true)
        end

        meta:set_string("infotext",
        "HV Transporter active (" .. eu_input .. "EU)")

        transporter_entrance_effect({x = pos.x, y = pos.y + 1, z = pos.z})
        --TODO: adjust pos and destination heights (+ - 1 node)
        transport_objects(pos, destination, meta)

        if minetest.is_yes(meta:get_string("bidirectional")) then
            transport_objects(destination, pos, meta)
            transporter_entrance_effect({
                x = destination.x,
                y = destination.y + 1,
                z = destination.z
            })
        end

    elseif enabled and eu_input < demand then

        if node.name == node_name_active then
            toggle_transporter(pos, false)
        end

        meta:set_string("infotext",
            "HV Transporter inactive (" .. demand .. "EU required)")

    else

        if node.name == node_name_active then
            toggle_transporter(pos, false)
        end

        meta:set_string("infotext", "HV Transporter disabled")

    end

end

-------------------------------------------------------------------------------
--Node registrations
-------------------------------------------------------------------------------

minetest.register_node(node_name, {
    description = "HV Transporter",
    tiles = {
        "transporter_top.png",
        "transporter_bottom.png",
        "transporter_side.png"
    },
    is_ground_content = false,
    groups = {
        oddly_breakable_by_hand = 3,
        technic_machine = 1,
        technic_hv = 1
    },
    connect_sides = {"bottom"},
    on_construct = transporter_on_construct,
    technic_run = transporter_on_technic_run,
    on_receive_fields = transporter_on_receive_fields,
})

minetest.register_node(node_name_active, {
    description = "HV Transporter active",
    tiles = {
        "transporter_top_active.png",
        "transporter_bottom.png",
        "transporter_side.png"
    },
    light_source = 14,
    is_ground_content = false,
    groups = {
        oddly_breakable_by_hand = 3,
        technic_machine = 1,
        technic_hv = 1,
        not_in_creative_inventory = 1
    },
    connect_sides = {"bottom"},
    drop = node_name,
    technic_run = transporter_on_technic_run,
    on_receive_fields = transporter_on_receive_fields,
})

minetest.register_craftitem(mod_name .. ":floppy", {
    description = "Transporter Storage",
    inventory_image = "transporter_floppy.png",
    stack_max = 1,
    range = 2.0,

    on_use = function(itemstack, user, pointed_thing)

        if pointed_thing.type == "node" and
            minetest.get_node(pointed_thing.under).name == node_name then

            local meta = minetest.get_meta(pointed_thing.under)

            itemstack:replace(mod_name .. ":floppy_filled")

            local item_meta = itemstack:get_meta()
            item_meta:set_string("bookmarks", meta:get_string("bookmarks"))
            return itemstack

        end
    end,
})

minetest.register_craftitem(mod_name .. ":floppy_filled", {
    description = "Filled Transporter Storage",
    inventory_image = "transporter_floppy_filled.png",
    groups = {
        not_in_creative_inventory = 1
    },
    stack_max = 1,
    range = 2.0,

    on_use = function(itemstack, user, pointed_thing)

        if pointed_thing.type == "node" and
        minetest.get_node(pointed_thing.under).name == node_name then

            local meta = minetest.get_meta(pointed_thing.under)
            local item_meta = itemstack:get_meta()

            local node_bookmarks = minetest.deserialize(
                meta:get_string("bookmarks"))
            local item_bookmarks = minetest.deserialize(
                itemmeta:get_string("bookmarks"))

            table.remove(item_bookmarks, 1)
            for k,v in pairs(item_bookmarks) do
                table.insert(node_bookmarks, v)
            end

            meta:set_string("bookmarks", minetest.serialize(node_bookmarks))
            update_node_formspec(meta)

        end
    end,
})

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

--TODO: Adjust crafting recipes for new technic_materials changes
minetest.register_craft({
    output = node_name,
    recipe = {
        {
            "technic:copper_coil",
            "technic:blue_energy_crystal",
            "technic:copper_coil"
        },
        {
            "technic:control_logic_unit",
            "technic:machine_casing",
            "technic:control_logic_unit"
        },
        {
            "technic:composite_plate",
            "technic:hv_cable",
            "technic:composite_plate"
        },
    }
})

minetest.register_craft({
    output = mod_name .. ":floppy",
    recipe = {
            {"technic:stainless_steel_ingot"},
            {"homedecor:plastic_sheeting"},
            {"default:paper"
        },
    }
})

technic.register_machine("HV", node_name, technic.receiver)
technic.register_machine("HV", node_name_active, technic.receiver)

