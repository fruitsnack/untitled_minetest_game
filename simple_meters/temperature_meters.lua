local mod_name = "simple_meters"

meters.register_meter({
    name = "thermometer",
    item = mod_name .. ":thermometer",
    on_update = function(player, element_id)

        local pos = player:get_pos()
        local stats = simple_temperature.get_stats_at(pos)

        if not stats then return end

        local ret = math.ceil(stats.heat / 10)

        player:hud_change(element_id,
        "text",
        mod_name .. "_thermometer_" .. ret .. ".png")
    end,

})

meters.register_meter({
    name = "humidity_meter",
    item = mod_name .. ":humidity_meter",
    on_update = function(player, element_id)

        local pos = player:get_pos()
        local stats = simple_temperature.get_stats_at(pos)

        if not stats then return end

        local ret = math.ceil(stats.heat_multiplier * 10)

        player:hud_change(element_id,
        "text",
        mod_name .. "_humidity_meter_" .. ret .. ".png")
    end,

})

minetest.register_craftitem(mod_name .. ":thermometer", {
    description = "Thermometer",
    inventory_image = mod_name .. "_thermometer_5.png",
    stack_max = 1,
})

minetest.register_craft({
    output = mod_name .. ":thermometer",
    recipe = {
        {"default:glass", "dye:dye_red", "default:glass"},
        {"default:glass", "bucket:bucket_water", "default:glass"},
        {"", "default:gold_ingot", ""},
    },
    replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}},
})


minetest.register_craftitem(mod_name .. ":humidity_meter", {
    description = "Humidity Meter",
    inventory_image = mod_name .. "_humidity_meter_5.png",
    stack_max = 1,
})

minetest.register_craft({
    output = mod_name .. ":humidity_meter",
    recipe = {
        {"default:glass", "dye:dye_blue", "default:glass"},
        {"default:glass", "bucket:bucket_water", "default:glass"},
        {"", "default:steel_ingot", ""},
    },
    replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}},
})
