-------------------------------------------------------------------------------
--Default Data Structures
-------------------------------------------------------------------------------

meters = {}
local player_context = {}
local hud_elements = {}

-------------------------------------------------------------------------------
--Settingtypes
-------------------------------------------------------------------------------

local mod_name = "simple_meters"

local function get_setting(name, def, bool)

    if bool then
        local inp = minetest.settings:get_bool(mod_name .. "_" .. name, def)
        return inp
    else
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return tonumber(inp)
    end

end

local update_interval = get_setting("update_interval", 0.5)
local hud_scale = get_setting("hud_scale", 6)
local hud_padding = get_setting("hud_padding", 10)

-------------------------------------------------------------------------------
--Utilities
-------------------------------------------------------------------------------

local function msg(level, input)
    minetest.log(level, "[" .. modname .. "] " .. input)
end

local function update_meters()

    for _,v in pairs(player_context) do

        for _,m in pairs(hud_elements) do

            if v.ref:get_inventory()
            :contains_item("main", {name = m.item}) then

                if v.hud_ids[m.name] then
                    m.on_update(v.ref, v.hud_ids[m.name])
                else
                    v.hud_ids[m.name] = v.ref:hud_add(m.hud_def)
                    m.on_update(v.ref, v.hud_ids[m.name])
                end

            else

                if v.hud_ids[m.name] then
                    v.ref:hud_remove(v.hud_ids[m.name])
                    v.hud_ids[m.name] = nil
                end

            end

        end

    end

end

-------------------------------------------------------------------------------
--API
-------------------------------------------------------------------------------

meters.register_meter = function(def)

    if not def.name or not def.item or not def.on_update then
        msg("error", "Missing meter definition fields!")
        return
    end

    local dir = 1
    local shift = (#hud_elements - (#hud_elements % 2))/2

    if #hud_elements % 2 == 0 then
        dir = -1
    end

    local base_offset = 275 * dir
    local offset = (shift * 16 * hud_scale * dir) + (shift * hud_padding * dir)

    local ret = {
        name = def.name,
        item = def.item,
        on_update = def.on_update,
        hud_def = {
            hud_elem_type = "image",
            position  = {x = 0.5, y = 1},
            offset    = {x = base_offset + offset, y = 0},
            text      = "",
            scale     = {x = hud_scale, y = hud_scale},
            alignment = {x = dir, y = -1},
        }
    }

    table.insert(hud_elements, ret)
end

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

local timer = 0
minetest.register_globalstep(function(dtime)

    timer = timer + dtime
    if timer < update_interval then return end
    update_meters()
    timer = 0

end)

minetest.register_on_joinplayer(function(player)
    local name = player:get_player_name()
    player_context[name] = {
        ref = player,
        hud_ids = {}
    }
end)

minetest.register_on_leaveplayer(function(player)
    local name = player:get_player_name()
    player_context[name] = nil
end)

local path = minetest.get_modpath("simple_meters")
dofile(path .. "/default_meters.lua")

if minetest.get_modpath("simple_temperature") then
    dofile(path .. "/temperature_meters.lua")
end
