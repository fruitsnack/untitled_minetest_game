local mod_name = "simple_meters"

meters.register_meter({
    name = "watch",
    item = mod_name .. ":watch",
    on_update = function(player, element_id)

        local time = minetest.get_timeofday() * 32

        player:hud_change(element_id,
        "text",
        mod_name .. "_clock_" .. math.ceil(time) .. ".png")

    end,

})

meters.register_meter({
    name = "compass",
    item = mod_name .. ":compass",
    on_update = function(player, element_id)

        local name = player:get_player_name()
        local pos = player:get_pos()
        local dir = math.deg(player:get_look_horizontal())

        local target = {x = 0, y = 0, z = 0}

        if beds.spawn[name] then
            target = beds.spawn[name]
        end

        local angle_to_north = math.deg(
        math.atan2(target.x - pos.x, target.z - pos.z))

        local angle = (angle_to_north + dir) % 360

        local id = math.ceil(angle/(360/16))

        player:hud_change(element_id,
        "text",
        mod_name .. "_compass_" .. id .. ".png")
    end,

})

minetest.register_craftitem(mod_name .. ":watch", {
    description = "Watch",
    inventory_image = mod_name .. "_clock_17.png",
    stack_max = 1,
})

minetest.register_craft({
    output = mod_name .. ":watch",
    recipe = {
        {"", "default:steel_ingot", ""},
        {"default:steel_ingot", "default:diamond", "default:steel_ingot"},
        {"", "default:steel_ingot", ""},
    },
})

minetest.register_craftitem(mod_name .. ":compass", {
    description = "Compass",
    inventory_image = mod_name .. "_compass_1.png",
    stack_max = 1,
})

minetest.register_craft({
    output = mod_name .. ":compass",
    recipe = {
        {"", "default:gold_ingot", ""},
        {"default:gold_ingot", "default:mese_crystal", "default:gold_ingot"},
        {"", "default:gold_ingot", ""},
    },
})
