ambience.register_sound(true, {
    sound = "test0",
    gain = 1,
    near = {"default:water_source", "default:water_flowing"},
    radius = 3
})

ambience.register_sound(true, {
    sound = "test1",
    gain = 1,
    near = {"default:sand", "default:leaves"},
    radius = 3
})

ambience.register_sound(true, {
    sound = "test2",
    gain = 1,
    near = {"default:dirt_with_grass"},
    radius = 3
})

ambience.register_sound(false, {
    sound = "test3",
    gain = 1,
    min_heat = 35,
    max_heat = 60,
    min_humidity = 45,
    max_humidity = 60,
    min_height = 0,
    max_height = 31000
})

ambience.register_sound(false, {
    sound = "test4",
    gain = 1,
    min_heat = 0,
    max_heat = 100,
    min_humidity = 0,
    max_humidity = 100,
    min_height = 15,
    max_height = 35
})

ambience.register_sound(false, {
    sound = "test5",
    gain = 1,
    min_heat = 35,
    max_heat = 60,
    min_humidity = 45,
    max_humidity = 60,
    min_height = 0,
    max_height = 31000
})
