-------------------------------------------------------------------------------
--Default data structures
-------------------------------------------------------------------------------

ambience = {}

local timed_sounds = {}
local environmental_sounds = {}
local player_context = {}

-------------------------------------------------------------------------------
--Settings
-------------------------------------------------------------------------------

local mod_name = "ambience"

local function get_setting(name, def, bool)

    if bool then
        local inp = minetest.settings:get_bool(mod_name .. "_" .. name, def)
        return inp
    else
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return tonumber(inp)
    end

end

local enable_timed = get_setting("timed_sounds", true, true)
local enable_environmental = get_setting("environmental_sounds", true, true)
local interval = get_setting("interval", 10)
local maximum_delay = get_setting("max_delay", 10)
local env_interval = get_setting("env_interval", 3)
local fade_time = get_setting("fade_time", 3)
local max_pitch_range = get_setting("pitch_range", 0.3)

-------------------------------------------------------------------------------
--Interal variables
-------------------------------------------------------------------------------

local delay = math.random(0, maximum_delay)

-------------------------------------------------------------------------------
--Util
-------------------------------------------------------------------------------

local function msg(level, input)
    minetest.log(level, "[" .. mod_name .. "] " .. input)
end

local function fastpow(a, b)
    local state = a
    for i = 1, b - 1 do
        state = state * a
    end
    return state
end

local function distance(pos1, pos2)
    return math.sqrt(
    fastpow(pos1.x - pos2.x, 2) +
    fastpow(pos1.y - pos2.y, 2) +
    fastpow(pos1.z - pos2.z, 2)
    )
end

local function process_timed()

   for k,v in pairs(player_context) do

       local pos = v.ref:get_pos()
       local heat = minetest.get_heat(pos)
       local humidity = minetest.get_humidity(pos)

       local fitting_sounds = {}

       for _,v1 in pairs(timed_sounds) do
           if pos.y > v1.min_height and pos.y < v1.max_height and
              heat > v1.min_heat and heat < v1.max_heat and
              humidity > v1.min_humidity and humidity < v1.max_humidity then
              table.insert(fitting_sounds, v1)
           end
       end

       if #fitting_sounds == 0 then return end

       local sound = fitting_sounds[math.random(#fitting_sounds)]
       local random_pitch = 1 + (math.random() - 0.5) * max_pitch_range

       minetest.sound_play(
       sound.sound,
       {
           to_player = k,
           gain = sound.gain,
           pitch = random_pitch,
       },
       true)

   end

end

local function process_environmental()

    for k,v in pairs(player_context) do

        local pos = v.ref:get_pos()

        for _,snd in pairs(v.sounds) do
            snd.valid = false
        end

        for _,v1 in pairs(environmental_sounds) do

            local nodepos = minetest.find_node_near(pos,
            v1.radius,
            v1.near,
            true)

            if nodepos and not v.sounds[v1.sound] then

                local handle = minetest.sound_play(v1.sound,
                {
                    to_player = k,
                    fade = (1 / fade_time) * v1.gain,
                    gain = v1.gain,
                    loop = true
                })

                v.sounds[v1.sound] = {
                    handle = handle,
                    valid = true,
                    gain = v1.gain
                }

            elseif nodepos and v.sounds[v1.sound] then

                v.sounds[v1.sound].valid = true

            end

        end

        for i,snd in pairs(v.sounds) do
            if snd.valid == false then
                minetest.sound_fade(snd.handle,
                -(1 / fade_time) * snd.gain,
                0.01)
                minetest.after(fade_time, function()
                    minetest.sound_stop(snd.handle)
                end)
                --TODO: make sure stuff doesnt pile up in the handle table
                v.sounds[i] = nil
            end
        end

    end

end

-------------------------------------------------------------------------------
--API
-------------------------------------------------------------------------------

ambience.register_sound = function(environmental, def)

    if (environmental and (not def.sound or not def.near))
    or (not environmental and not def.sound) then
        msg("error", "missing sound definition parameters!")
        return
    end

    local tdef = {}

    if environmental then

        tdef = {
            sound = def.sound,
            gain = def.gain or 1,
            near = def.near,
            radius = def.radius or 3
        }

        table.insert(environmental_sounds, tdef)

    else

        tdef = {
            sound = def.sound,
            gain = def.gain or 1,
            min_heat = def.min_heat or 0,
            max_heat = def.max_heat or 100,
            min_humidity = def.min_humidity or 0,
            max_humidity = def.max_humidity or 100,
            min_height = def.min_height or -31000,
            max_height = def.max_height or 31000
        }

        table.insert(timed_sounds, tdef)

    end
end

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

local timer = 0
if enable_timed then
    minetest.register_globalstep(function(dtime)
            timer = timer + dtime
            if timer < interval + delay then return end
            print(interval, delay)
            delay = math.random(0, maximum_delay)
            process_timed()
            timer = 0
    end)
end

local env_timer = 0
if enable_environmental then
    minetest.register_globalstep(function(dtime)
            env_timer = env_timer + dtime
            if env_timer < env_interval then return end
            process_environmental()
            env_timer = 0
    end)
end

minetest.register_on_joinplayer(function(player)
   local name = player:get_player_name()
   if not name then return end
   player_context[name] = {ref = player, sounds = {}}
end)

minetest.register_on_leaveplayer(function(player)
    local name = player:get_player_name()
    if not name then return end
    player_context[name] = nil
end)
