local ymax = 31000
local ymin = -31000
local gain = 1

music.register_track({
    name = "parting_of_the_ways",
    length = 396,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "dreams_become_real",
    length = 405,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})
