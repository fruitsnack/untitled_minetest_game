local ymax = 31000
local ymin = -8
local gain = 1

music.register_track({
    name = "water_lily",
    length = 126,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "gymnopedie_no_1",
    length = 187,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "at_rest",
    length = 205,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "white",
    length = 179,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "feather_waltz",
    length = 85,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})
