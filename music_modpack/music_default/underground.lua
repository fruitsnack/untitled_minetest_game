local ymax = -8
local ymin = -31000
local gain = 1

music.register_track({
    name = "snowdrop",
    length = 273,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "road_to_hell",
    length = 108,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "rites",
    length = 126,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "unnatural_situation",
    length = 156,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "despair_and_triumph",
    length = 281,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})

music.register_track({
    name = "heartbreaking",
    length = 93,
    gain = gain,
    day = true,
    night = true,
    ymin = ymin,
    ymax = ymax,
})
