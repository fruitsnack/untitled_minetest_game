# Description
This directory contains project files for my minetest mods.  
Right now it's a huge disorganized mess.  

## Requirements
Assets were produced using the following software:
- Ardour 6 & 7 (various audio samples)
- Audacity (various audio samples)
- blender (.blend)
- krita (.psd and .kra)
- gimp (.xcf)

## License
Visual assets in `simple emojis` directory were made by Tempodrone and released under GPLv3 [link to the license](https://www.gnu.org/licenses/gpl-3.0.en.html)
All other visual assets (sprites, textures, etc) are made by me (Fruitsnack) and licensed under Creative Commons 4.0 Attribution-NonCommercial-NoDerivs 4.0 International (CC BY-NC-ND) [refer to this link for more info](https://creativecommons.org/licenses/by-nc-nd/4.0/).

All audio assets located in `music_modpack` directory are produced by Kevin McLeod (incompetech.com) and are licensed under Creative Commons: By Attribution 4.0 License (https://creativecommons.org/licenses/by/4.0).

All other audio assets are remixed, cut and overall processed from sound samples from freesound.org. Only audio assets, licensed under CC0 were used, however they're still credited in their respective mod folders. Audio assets are licensed under Creative Commons 4.0 Attribution-NonCommercial-NoDerivs 4.0 International (CC BY-NC-ND) [refer to this link for more info](https://creativecommons.org/licenses/by-nc-nd/4.0/).
