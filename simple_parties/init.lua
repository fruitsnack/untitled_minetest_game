-------------------------------------------------------------------------------
--Data structures
-------------------------------------------------------------------------------

parties = {}

local data = {}
data.player_context = {}
data.party_list = {}

local formspec_temp = {}
local inventories = {}

-------------------------------------------------------------------------------
--Settingtype initialization
-------------------------------------------------------------------------------

local mod_name = "parties"

local function get_setting(name, def)

    if type(def) == "boolean" then
        local inp = minetest.settings:get_bool(mod_name .. "_" .. name, def)
        return inp
    elseif type(def) == "string" then
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return inp
    else
        local inp = minetest.settings:get(mod_name .. "_" .. name) or def
        return tonumber(inp)
    end

end

local autosave_interval = get_setting("autosave_interval", 60)
local enable_autosave = get_setting("autosave", true)
local max_description_size = get_setting("description_size", 256)
local max_party_name_size = get_setting("max_party_name_size", 16)
local min_party_name_size = get_setting("min_party_name_size", 4)
local left_name_separator = get_setting("left_separator", "[")
local right_name_separator = get_setting("right_separator", "]")
local party_chest_size = get_setting("party_chest_size", 48)

-------------------------------------------------------------------------------
--Internal variables
-------------------------------------------------------------------------------

local ranks = {"Member", "Manager", "Leader", "Founder"}
local statuses = {"Offline", "Online"}
local rank_colors = {"#FFFFFF", "#30FF80", "#8030FF", "#FF8030"}
local status_colors = {"#FF8010", "#10FF80"}
local message_color = "#50FF80"
local notification_color = "#FF8050"

-------------------------------------------------------------------------------
--Utilities
-------------------------------------------------------------------------------

local function msg(level, input)
    minetest.log(level, "[" .. mod_name .. "] " .. input)
end

-------------------------------------------------------------------------------
--Setting loading and saving
-------------------------------------------------------------------------------

local function load_party_settings()

    local file = io.open(minetest.get_worldpath() ..
    "/" ..
    mod_name ..
    ".mt",
    "r")

    if file then
        local rawfile = file:read()
        io.close(file)
        if rawfile then
            local input = minetest.deserialize(rawfile)
            data = input.data

            for k,v in pairs(input.inv) do

                inventories[k] = minetest
                .create_detached_inventory(mod_name .. k)
                inventories[k]:set_size("main", party_chest_size)

                local inv_list = {}

                for i,stack in pairs(v) do
                    inv_list[i] = ItemStack(stack)
                end

                inventories[k]:set_list("main", inv_list)

            end

        else
            msg("error", "Unable to read party data!")
        end
    end

end

local function save_party_settings()

    local path = minetest.get_worldpath() ..
    "/" ..
    mod_name ..
    ".mt"

    local file = io.open(path, "w")

    local serialized_inv = {}

    for k,v in pairs(inventories) do
        serialized_inv[k] = {}
        for i,stack in pairs(v:get_list("main")) do
            serialized_inv[k][i] = stack:to_string()
        end
    end

    if file then
        local rawfile = minetest.serialize({data = data, inv = serialized_inv})
        file:write(rawfile)
        io.close(file)
    else
        msg("error", "Unable to save party data!")
    end

end

local timer = 0

if enable_autosave then

    minetest.register_globalstep(function(dtime)

            timer = timer + dtime
            if timer < autosave_interval then return end

            if minetest.get_connected_players() == nil then return end
            save_party_settings()

            timer = 0
    end)

end

minetest.register_on_shutdown(save_party_settings)
load_party_settings()

for _,v in pairs(data.player_context) do
    v.status = 1
end

-------------------------------------------------------------------------------
--Nametag utils
-------------------------------------------------------------------------------

local function add_nametag_info(name, party_name)

    local player = minetest.get_player_by_name(name)
    if not player then return end

    local color = data.party_list[party_name].name_color

    local result = {}

    result[#result + 1] = left_name_separator
    result[#result + 1] = party_name
    result[#result + 1] = right_name_separator
    result[#result + 1] = " "
    result[#result + 1] = name

    player:set_nametag_attributes(
    {
        color = {r = color.r, g = color.g, b = color.b, a = 255},
        text = table.concat(result)
    })
end

local function clear_nametag_info(name)

    local player = minetest.get_player_by_name(name)
    if not player then return end

    player:set_nametag_attributes(
    {
        color = {r=255, g=255, b=255, a=255},
        text = player:get_player_name()
    })
end

local function update_party_nametags(party_name)

    for _,v in pairs(data.party_list[party_name].members) do

        local player = minetest.get_player_by_name(v.name)
        add_nametag_info(v.name, party_name)

    end

end

local function clear_party_nametags(party_name)

    for _,v in pairs(data.party_list[party_name].members) do

        clear_nametag_info(v.name)

    end

end

local function test_alphanumeric(input)
    if type(input) == "string" then
        if string.match(input, "[^0-9a-zA-Z%s]") then
            return false
        else
            return true
        end
    end
end

-------------------------------------------------------------------------------
--Formspec building
-------------------------------------------------------------------------------

local function scrollbar_to_color(input)
    return (input / 1000) * 255
end

local function color_to_scrollbar(input)
    return (input / 255) * 1000
end

local function build_no_party_formspec()

    local formspec = {}

    formspec[#formspec + 1] = "field_close_on_enter["
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_new_party_field;false]"

    formspec[#formspec + 1] = "field[2.79,3.75;3,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_new_party_field;Create a new party:;]"

    formspec[#formspec + 1] = "button[2.5,4.25;3,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_new_party_button;Create]"

    return table.concat(formspec)

end

local function build_invite_formspec(name)

    formspec_temp[name].join_list = data.player_context[name].invites

    local formspec = {}
    local invite_list = {}

    for _,v in pairs(formspec_temp[name].join_list) do

        local color = data.party_list[v].name_color

        invite_list[#invite_list + 1] = minetest.rgba(
        color.r,
        color.g,
        color.b)

        invite_list[#invite_list + 1] = minetest.formspec_escape(v)
    end

    formspec[#formspec + 1] = "tablecolumns["
    formspec[#formspec + 1] = "color,span=1;text,align=left,width=10]"

    formspec[#formspec + 1] = "table[2.39,2.5;3,3;"
    formspec[#formspec + 1] =  mod_name
    formspec[#formspec + 1] =  "_join_invite_list;"
    formspec[#formspec + 1] = table.concat(invite_list, ",")
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "button[2.39,5.6;1.65,1;"
    formspec[#formspec + 1] =  mod_name
    formspec[#formspec + 1] = "_join_party;Join]"

    formspec[#formspec + 1] = "button[3.95,5.6;1.65,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_cancel_join_party;Reject]"

    formspec[#formspec + 1] = "label[2.39,2.1;You've been invited to:]"

    return table.concat(formspec)
end

local function build_party_formspec(name)

    local current_player = data.player_context[name]
    local current_party = current_player.party

    formspec_temp[name].member_list = data.party_list[current_party].members
    formspec_temp[name].invite_list = data.party_list[current_party].invites

    local formspec = {}
    local user_list = {}
    local invite_list = {}

    for _,v in pairs(formspec_temp[name].member_list) do

        user_list[#user_list + 1] = minetest.formspec_escape(v.name)
        user_list[#user_list + 1] = rank_colors[v.rank]
        user_list[#user_list + 1] = ranks[v.rank]
        user_list[#user_list + 1] = status_colors[
        data.player_context[v.name].status]
        user_list[#user_list + 1] = statuses[
        data.player_context[v.name].status]

    end

    for _,v in pairs(formspec_temp[name].invite_list) do
        invite_list[#invite_list + 1] = minetest.formspec_escape(v)
    end

    local color = data.party_list[current_party].name_color
    local party_info = data.party_list[current_party].info

    formspec[#formspec + 1] = "field_close_on_enter["
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_invite_field;false]"

    formspec[#formspec + 1] = "field_close_on_enter["
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_rename_field;false]"

    formspec[#formspec + 1] = "tablecolumns["
    formspec[#formspec + 1] = "text,align=left,width=3;"
    formspec[#formspec + 1] = "color,span=1;"
    formspec[#formspec + 1] = "text,align=right,width=4;"
    formspec[#formspec + 1] = "color,span=1;"
    formspec[#formspec + 1] = "text,align=right,width=5]"

    formspec[#formspec + 1] = "table[0,0.20;3.10,4.25;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_member_list;"
    formspec[#formspec + 1] = table.concat(user_list, ",")
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "tablecolumns[text,align=left,width=10]"

    formspec[#formspec + 1] = "table[0,4.9;3.10,2.6;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_invite_list;"
    formspec[#formspec + 1] = table.concat(invite_list, ",")
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "field[0.3,8.22;3.3,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_invite_field;;]"

    formspec[#formspec + 1] = "button[3.2,7.9;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_invite;Invite]"

    formspec[#formspec + 1] = "button[3.2,4.82;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_cancel_invite;Cancel]"

    formspec[#formspec + 1] = "button[3.2,0.12;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_promote;Promote]"

    formspec[#formspec + 1] = "button[3.2,0.9;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_demote;Demote]"

    formspec[#formspec + 1] = "button[3.2,1.68;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_set_founder;Founder]"

    formspec[#formspec + 1] = "button[3.2,3.68;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_kick;Kick]"

    formspec[#formspec + 1] = "button[6.5,7.9;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_leave;Leave]"

    formspec[#formspec + 1] = "button[5.1,7.9;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_disband;Disband]"

    formspec[#formspec + 1] = "label[0,-0.25;Members:]"

    formspec[#formspec + 1] = "label[0,4.45;Invites:]"

    formspec[#formspec + 1] = "label[0,7.525;Invite Player:]"

    formspec[#formspec + 1] = "label[6.4,0.15;"
    formspec[#formspec + 1] = minetest.colorize("#FF0000", "R")
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "label[6.4,0.515;"
    formspec[#formspec + 1] = minetest.colorize("#00FF00", "G")
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "label[6.4,0.88;"
    formspec[#formspec + 1] = minetest.colorize("#0000FF", "B")
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "label[5.1,-0.25;"
    formspec[#formspec + 1] = "Name: "
    formspec[#formspec + 1] = minetest.colorize(
    minetest.rgba(color.r, color.g, color.b),
    minetest.formspec_escape(current_party))
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "scrollbar[5.1,0.215;2.685,0.325;horizontal;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_col_r;"
    formspec[#formspec + 1] = color_to_scrollbar(color.r)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "scrollbar[5.1,0.58;2.685,0.325;horizontal;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_col_g;"
    formspec[#formspec + 1] = color_to_scrollbar(color.g)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "scrollbar[5.1,0.945;2.685,0.325;horizontal;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_col_b;"
    formspec[#formspec + 1] = color_to_scrollbar(color.b)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "button[6.5,1.285;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_set_color;Set]"

    formspec[#formspec + 1] = "field[5.392,3.2;2.893,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_rename_field;Rename:;"
    formspec[#formspec + 1] = minetest.formspec_escape(current_party)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "button[6.5,3.68;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_rename_button;Rename]"

    formspec[#formspec + 1] = "textarea[5.392,4.875;2.893,2.4;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_party_info;Information:;"
    formspec[#formspec + 1] = minetest.formspec_escape(party_info)
    formspec[#formspec + 1] = "]"

    formspec[#formspec + 1] = "button[6.5,6.87;1.5,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_save_info;Save]"

    return table.concat(formspec)
end

local function show_confirm_dialog(name, prefix, message)

    local formspec = {}

    formspec[#formspec + 1] = "size[5,2]"
    formspec[#formspec + 1] = default.gui_bg
    formspec[#formspec + 1] = default.gui_bg_img

    formspec[#formspec + 1] = "textarea[1.4,0.75;5,1;;"
    formspec[#formspec + 1] = message
    formspec[#formspec + 1] = ";]"

    formspec[#formspec + 1] = "button_exit[0,1.35;1.3,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_"
    formspec[#formspec + 1] = prefix
    formspec[#formspec + 1] = "_confirm_button;Confirm]"

    formspec[#formspec + 1] = "button_exit[3.7,1.35;1.3,1;"
    formspec[#formspec + 1] = mod_name
    formspec[#formspec + 1] = "_"
    formspec[#formspec + 1] = prefix
    formspec[#formspec + 1] = "_cancel_button;Cancel]"

    minetest.show_formspec(name,
    mod_name .. "_" .. prefix .. "_confirm_dialog",
    table.concat(formspec))
end

local function get_party_formspec(self, player, context)

    local name = player:get_player_name()
    local party_formspec

    if not data.player_context[name].party then

        if #data.player_context[name].invites == 0 then
            party_formspec = build_no_party_formspec()
        else
            party_formspec = build_invite_formspec(name)
        end

    else
        party_formspec = build_party_formspec(name)
    end

    return sfinv.make_formspec(player,
    context,
    party_formspec,
    false,
    "size[8,8.6]")
end

-------------------------------------------------------------------------------
--Util
-------------------------------------------------------------------------------

local function user_message(name, message)

    local message = minetest.colorize(message_color,
    "<" .. mod_name .. "> " .. message)

    minetest.chat_send_player(name, message)

end

local function send_notification(name, message)

    local message = minetest.colorize(notification_color,
    "<" .. mod_name .. "> " .. message)

    minetest.chat_send_player(name, message)

    minetest.sound_play(mod_name .. "_notification",
    {
        to_player = name,
        gain = 1
    })

end

local function get_rank(name)

    local player_info = data.player_context[name]

    if not player_info or player_info and not player_info.party then
        return
    end

    for _,v in pairs(data.party_list[player_info.party].members) do

        if v.name == name then
            return v.rank
        end

    end

end

local function test_privileges(name, minimum, nomsg)

    local player_info = data.player_context[name]

    if not player_info or player_info and not player_info.party then
        return
    end

    local member_list = data.party_list[player_info.party].members

    local rank = 0

    for k,v in pairs(member_list) do

        if v.name == name then
            rank = v.rank
            break
        end

    end

    if rank >= minimum then

        return true

    else

        if not nomsg then
            user_message(name,
            "You need to have rank of " .. ranks[minimum] .. "!")
        end

        return false

    end

end

local function test_party_name(party_name, name)

    if not test_alphanumeric(party_name) then

        user_message(name,
        "Party name should contain only alphanumeric characters!")

        return false
    end

    if not party_name or party_name and
    (string.len(party_name) > max_party_name_size or
    string.len(party_name) < min_party_name_size) then

        user_message(name, "Party name should be between " ..
        min_party_name_size ..
        " and " ..
        max_party_name_size ..
        " characters!")

        return false
    end

    if data.party_list[party_name] then

        user_message(name, "Party with such name already exists!")
        return false

    else

        return true

    end

end

local function update_all_formspecs()

    local players = minetest.get_connected_players()

    for k,v in pairs(players) do

        if sfinv.get_page(v) == mod_name .. ":parties" then
            sfinv.set_page(v, mod_name .. ":parties")
        end

    end

end

local function update_page(player)
    if not player then return end
    sfinv.set_page(player, mod_name .. ":parties")
end

local function update_party_formspecs(party_name)

    for k,v in pairs(data.party_list[party_name].members) do

        local player = minetest.get_player_by_name(v.name)

        if player then

            if sfinv.get_page(player) == mod_name .. ":parties" then
                sfinv.set_page(player, mod_name .. ":parties")
            end

        end

    end

end

local function clear_invite_in_parties(name)

    for _,v in pairs(data.party_list) do

        for k,inv in pairs(v.invites) do

            if inv == name then
                table.remove(v.invites, k)
            end

        end

    end

end

-------------------------------------------------------------------------------
--Formspec actions
-------------------------------------------------------------------------------

local function formspec_action(self, player, context, fields)

    if not player then return end

    local name = player:get_player_name()
    local player_info = data.player_context[name]
    local party_info = data.party_list[player_info.party]

    ---------------------------------------------------------------------------
    --New Party
    ---------------------------------------------------------------------------

    if fields[mod_name .. "_new_party_button"] or
    (fields.key_enter_field and
    fields.key_enter_field == mod_name .. "_new_party_field") then

        if player_info.party then return end

        local party_name = fields[mod_name .. "_new_party_field"]

        if not test_party_name(party_name, name) then return end

        data.party_list[party_name] = {
            members = {{name = name, rank = 4}},
            invites = {},
            name_color = {r = 255, g = 255, b = 255},
            info = "",
        }

        data.player_context[name].party = party_name
        data.player_context[name].invites = {}

        inventories[party_name] = minetest
        .create_detached_inventory(mod_name .. party_name)

        inventories[party_name]:set_size("main", party_chest_size)

        clear_invite_in_parties(name)
        add_nametag_info(name, party_name)

        update_all_formspecs()

    ---------------------------------------------------------------------------
    --Join Party
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_join_party"] then

        if player_info.party then return end
        if not formspec_temp[name].join_list_id then return end

        local selected_party = formspec_temp[name]
        .join_list[formspec_temp[name].join_list_id]

        if not data.party_list[selected_party] then return end

        player_info.party = selected_party
        player_info.invites = {}

        table.insert(data.party_list[selected_party].members,
        {
            name = name,
            rank = 1
        })

        for k,v in pairs(data.party_list[selected_party].invites) do

            if v == name then
                table.remove(data.party_list[selected_party].invites, k)
            end

        end

        add_nametag_info(name, selected_party)
        clear_invite_in_parties(name)

        update_all_formspecs()

    ---------------------------------------------------------------------------
    --Cancel Party Join
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_cancel_join_party"] then

        if party_info then return end
        if not formspec_temp[name].join_list_id then return end

        local selected_invite = formspec_temp[name]
        .join_list[formspec_temp[name].join_list_id]

        local selected_party = data.party_list[selected_invite]

        if not selected_invite or not selected_party then return end

        for k,v in pairs(selected_party.invites) do

            if v == name then
                table.remove(selected_party.invites, k)
            end

        end

        for k,v in pairs(player_info.invites) do

            if v == selected_invite then
                table.remove(player_info.invites, k)
            end

        end

        if formspec_temp[name].join_list_id >
        #formspec_temp[name].join_list and
        formspec_temp[name].join_list_id > 1 then

            formspec_temp[name].join_list_id =
            formspec_temp[name].join_list_id - 1

        end

        update_party_formspecs(selected_invite)
        update_page(player)

    ---------------------------------------------------------------------------
    --Set Party Color
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_set_color"] then

        if not party_info then return end
        if not test_privileges(name, 3) then return end

        local R = scrollbar_to_color(
        minetest.explode_scrollbar_event(fields[mod_name .. "_col_r"]).value)
        local G = scrollbar_to_color(
        minetest.explode_scrollbar_event(fields[mod_name .. "_col_g"]).value)
        local B = scrollbar_to_color(
        minetest.explode_scrollbar_event(fields[mod_name .. "_col_b"]).value)

        party_info.name_color = {r = R, g = G, b = B}

        update_party_nametags(player_info.party)
        update_party_formspecs(player_info.party)

    ---------------------------------------------------------------------------
    --Rename Party
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_rename_button"] or
    (fields.key_enter_field and
    fields.key_enter_field == mod_name .. "_rename_field") then

        if not party_info then return end
        if not test_privileges(name, 4) then return end

        local input = fields[mod_name .. "_rename_field"]
        local old_name = player_info.party

        if not test_party_name(input, name) then return end

        for _,v in pairs(data.player_context) do

            if v.party == old_name then
                v.party = input
            end

            for k,inv in pairs(v.invites) do

                if inv == old_name then
                    v.invites[k] = input
                end

            end

        end

        data.party_list[input] = data.party_list[old_name]

        local lists = inventories[old_name]:get_lists()
        inventories[input] = minetest
        .create_detached_inventory(mod_name .. input)
        inventories[input]:set_lists(lists)
        minetest.remove_detached_inventory(mod_name .. old_name)

        data.party_list[old_name] = nil

        update_party_nametags(input)
        update_party_formspecs(player_info.party)

    ---------------------------------------------------------------------------
    --Invite Player
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_invite"] or
    (fields.key_enter_field and
    fields.key_enter_field == mod_name .. "_invite_field") then

        if not party_info then return end
        if not test_privileges(name, 2) then return end

        local invite_name = fields[mod_name .. "_invite_field"]

        if invite_name == name then

            user_message(name, "You can't send an invite to yourself!")
            return

        end

        if not minetest.player_exists(invite_name) then

            user_message(name, "Can't find this player!")
            return

        elseif not data.player_context[invite_name] then

            data.player_context[invite_name] =
            {
                party = nil,
                invites =  {},
                status = 1
            }

        end

        if data.player_context[invite_name].party ~= nil then

            user_message(name,
            "Player is already in a party! Ask him to leave first.")
            return

        end

        for k,v in pairs(party_info.invites) do

            if v == invite_name then

                user_message(name, "Player is already invited!")
                return

            end

        end

        table.insert(data.player_context[invite_name].invites,
        player_info.party)
        table.insert(party_info.invites, invite_name)

        send_notification(invite_name,
        "You've been invited to join " ..
        player_info.party ..
        "! Check \"Parties\" tab in your inventory")

        update_party_formspecs(player_info.party)
        update_page(minetest.get_player_by_name(invite_name))

    ---------------------------------------------------------------------------
    --Cancel Invite
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_cancel_invite"] then

        if not party_info then return end
        if not test_privileges(name, 2) then return end
        if not formspec_temp[name].invite_list_id then return end

        local inv_name = formspec_temp[name]
        .invite_list[formspec_temp[name].invite_list_id]

        if not inv_name or not data.player_context[inv_name] then return end

        for k,v in pairs(party_info.invites) do
            if v == inv_name then
                table.remove(party_info.invites, k)
            end
        end

        for k,v in pairs(data.player_context[inv_name].invites) do
            if v == player_info.party then
                table.remove(data.player_context[inv_name].invites, k)
            end
        end

        if formspec_temp[name].invite_list_id >
        #formspec_temp[name].invite_list and
        formspec_temp[name].invite_list_id > 1 then

            formspec_temp[name].invite_list_id =
            formspec_temp[name].invite_list_id - 1

        end

        send_notification(inv_name,
        "Your invitation to " .. player_info.party .. " was canceled!")

        update_party_formspecs(player_info.party)

    ---------------------------------------------------------------------------
    --Save Party Info
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_save_info"] or
    (fields.key_enter_field and
    fields.key_enter_field == mod_name .. "_party_info") then

        if not party_info then return end
        if not test_privileges(name, 3) then return end

        local input = fields[mod_name .. "_party_info"]

        if not input or input and string.len(input) > max_description_size then

            user_message(name,
            "Maximum allowed size of the description is " ..
            max_description_size ..
            "!")

            return
        end

        party_info.info = input

        update_party_formspecs(player_info.party)

    ---------------------------------------------------------------------------
    --Promote
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_promote"] then

        if not party_info then return end
        if not formspec_temp[name].member_list_id then return end

        local selected_name = formspec_temp[name]
        .member_list[formspec_temp[name].member_list_id].name

        if not selected_name then return end

        if not data.player_context[selected_name] then return end
        if selected_name == name then return end

        local player_rank = get_rank(name)
        local selected_rank = get_rank(selected_name)

        if selected_rank + 1 >= player_rank then return end

        for k,v in pairs(party_info.members) do

            if v.name == selected_name then
                v.rank = v.rank + 1
                break
            end

        end

        send_notification(selected_name, "You've been promoted!")

        update_party_formspecs(player_info.party)

    ---------------------------------------------------------------------------
    --Demote
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_demote"] then

        if not party_info then return end
        if not formspec_temp[name].member_list_id then return end

        local selected_name = formspec_temp[name]
        .member_list[formspec_temp[name].member_list_id].name

        if not selected_name then return end
        if not data.player_context[selected_name] then return end

        if selected_name == name then return end

        local player_rank = get_rank(name)
        local selected_rank = get_rank(selected_name)

        if selected_rank >= player_rank or selected_rank - 1 <= 0 then
            return
        end

        for k,v in pairs(party_info.members) do

            if v.name == selected_name then
                v.rank = v.rank - 1
                break
            end

        end

        send_notification(selected_name, "You've been demoted!")

        update_party_formspecs(player_info.party)

    ---------------------------------------------------------------------------
    --Disband
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_disband"] then

        if not party_info then return end
        if not test_privileges(name, 4) then return end

        show_confirm_dialog(name, "disband",
        "Are you sure you want to disband " ..
        player_info.party ..
        "?")

    ---------------------------------------------------------------------------
    --Set Founder
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_set_founder"] then

        if not party_info then return end
        if not test_privileges(name, 4) then return end
        if not formspec_temp[name].member_list_id then return end

        local selected_name = formspec_temp[name]
        .member_list[formspec_temp[name].member_list_id].name

        if not selected_name then return end
        if not data.player_context[selected_name] then return end

        if selected_name == name then

            user_message(name, "You're already a founder!")
            return

        end

        local player_rank = get_rank(name)
        local selected_rank = get_rank(selected_name)

        show_confirm_dialog(name, "set_founder",
        "Are you sure you want to transfer ownership to " ..
        selected_name ..
        "?")

    ---------------------------------------------------------------------------
    --Kick
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_kick"] then

        if not party_info then return end
        if not formspec_temp[name].member_list_id then return end

        local selected_name = formspec_temp[name]
        .member_list[formspec_temp[name].member_list_id].name

        if not selected_name then return end
        if not data.player_context[selected_name] then return end

        if selected_name == name then

            user_message(name, "You can't kick yourself!")
            return

        end

        local player_rank = get_rank(name)
        local selected_rank = get_rank(selected_name)

        if selected_rank >= player_rank then return end

        show_confirm_dialog(name, "kick",
        "Are you sure you want to kick " ..
        selected_name ..
        "?")

    ---------------------------------------------------------------------------
    --Leave
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_leave"] then

        if not party_info then return end

        if test_privileges(name, 4, true) and #party_info.members ~= 1 then

            user_message(name, "Founder can only leave if there's no members!")
            return

        end

        show_confirm_dialog(name, "leave",
        "Are you sure you want to leave " ..
        player_info.party ..
        "?")

    ---------------------------------------------------------------------------
    --Select table element in invite table
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_invite_list"] then

        if not party_info then return end

        local event = minetest.explode_table_event(
        fields[mod_name .. "_invite_list"])

        if event.type == "INV" then

            formspec_temp[name].invite_list_id = nil

        else

            formspec_temp[name].invite_list_id = event.row

        end

        update_page(player)

    ---------------------------------------------------------------------------
    --Select table element in member table
    ---------------------------------------------------------------------------

    elseif fields[mod_name .."_member_list"] then

        if not party_info then return end

        local event = minetest.explode_table_event(
        fields[mod_name .. "_member_list"])

        if event.type == "INV" then

            formspec_temp[name].member_list_id = nil

        else

            formspec_temp[name].member_list_id = event.row

        end

        update_page(player)

    ---------------------------------------------------------------------------
    --Select table element in join invite table
    ---------------------------------------------------------------------------

    elseif fields[mod_name .. "_join_invite_list"] then

        if party_info then return end

        local event = minetest.explode_table_event(
        fields[mod_name .. "_join_invite_list"])

        if event.type == "INV" then

            formspec_temp[name].join_list_id = nil

        else

            formspec_temp[name].join_list_id = event.row

        end

        update_page(player)

    end
end

-------------------------------------------------------------------------------
--Confirmation dialog actions
-------------------------------------------------------------------------------

minetest.register_on_player_receive_fields(function(player, form_name, fields)

    if form_name == mod_name .. "_leave_confirm_dialog" or
       form_name == mod_name .. "_kick_confirm_dialog" or
       form_name == mod_name .. "_set_founder_confirm_dialog" or
       form_name == mod_name .. "_disband_confirm_dialog" then

        local name = player:get_player_name()
        local player_info = data.player_context[name]
        local party_info = data.party_list[player_info.party]

        if not party_info then return end

        -----------------------------------------------------------------------
        --Leave
        -----------------------------------------------------------------------

        if fields[mod_name .. "_leave_confirm_button"] then

            if test_privileges(name, 4, true) and
            #party_info.members ~= 1 then return end

            for k,v in pairs(party_info.members) do

                if v.name == name then
                    table.remove(party_info.members, k)
                end

            end

            if #party_info.members == 0 then

                minetest
                .remove_detached_inventory(mod_name .. player_info.party)
                inventories[player_info.party] = nil

                data.party_list[player_info.party] = nil

                for _,v in pairs(data.player_context) do

                    for k,inv in pairs(v.invites) do

                        if player_info.party == inv then
                            table.remove(v.invites, k)
                        end

                    end

                end

            end

            player_info.party = nil
            clear_nametag_info(name)

            update_page(player)

        -----------------------------------------------------------------------
        --Set Founder
        -----------------------------------------------------------------------

        elseif fields[mod_name .. "_set_founder_confirm_button"] then

            if not test_privileges(name, 4) or
            not formspec_temp[name].member_list_id then return end

            local selected_name = formspec_temp[name]
            .member_list[formspec_temp[name].member_list_id].name

            if not selected_name then return end
            if not data.player_context[selected_name] then return end
            if selected_name == name then return end

            for _,v in pairs(party_info.members) do

                if v.name == name then

                    v.rank = 3

                elseif v.name == selected_name then

                    v.rank = 4

                end

            end

            send_notification(selected_name,
            "You've been promoted to founder!")
            update_party_formspecs(player_info.party)

        -----------------------------------------------------------------------
        --Kick
        -----------------------------------------------------------------------

        elseif fields[mod_name .. "_kick_confirm_button"] then

            if not formspec_temp[name].member_list_id then return end

            local selected_name = formspec_temp[name]
            .member_list[formspec_temp[name].member_list_id].name

            if not selected_name then return end
            if not data.player_context[selected_name] then return end
            if selected_name == name then return end

            local player_rank = get_rank(name)
            local selected_rank = get_rank(selected_name)

            if selected_rank >= player_rank then return end

            for k,v in pairs(party_info.members) do

                if v.name == selected_name then

                    table.remove(party_info.members, k)

                    break

                end

            end

            send_notification(selected_name,
            "You've been kicked from " ..
            party_info.party ..
            "!")

            clear_nametag_info(selected_name)
            update_party_formspecs(party_info.party)

            data.player_context[selected_name].party = nil
            update_page(minetest.get_player_by_name(selected_name))

        -----------------------------------------------------------------------
        --Disband
        -----------------------------------------------------------------------

        elseif fields[mod_name .. "_disband_confirm_button"] then

            if not test_privileges(name, 4, true) then return end

            local party_name = player_info.party

            minetest.remove_detached_inventory(mod_name .. party_name)
            inventories[party_name] = nil

            for _,v in pairs(data.player_context) do
                for k,inv in pairs(v.invites) do
                    if inv == party_name then
                        table.remove(v.invites, k)
                    end
                end
            end

            for _,v in pairs(party_info.members) do
                data.player_context[v.name].party = nil
                send_notification(v.name, party_name .. " was disbanded!")
            end

            clear_party_nametags(party_name)
            data.party_list[party_name] = nil

            update_all_formspecs()

        end

    end

end)

-------------------------------------------------------------------------------
--API
-------------------------------------------------------------------------------

parties.get_player_party = function(name)

    if not minetest.player_exists(name) then
        msg("error", "Player " .. name .. " doesn't exist!")
        return
    end

    if data.player_context[name] and data.player_context[name].party then

        return data.player_context[name].party

    else

        return nil

    end

end

parties.get_player_invites = function(name)

    if not minetest.player_exists(name) then
        msg("error", "Player " .. name .. " doesn't exist!")
        return
    end

    if data.player_context[name] and data.player_context[name].invites then

        return data.player_context[name].invites

    else

        return nil

    end

end

parties.get_party_info = function(party_name)

    if not data.party_list[party_name] then
        msg("error", "Party " .. party_name .. " doesn't exist!")
        return
    end

    return data.party_list[party_name]

end

parties.get_party_list = function()

    local list = {}

    for k,_ in pairs(data.party_list) do
        list[#list + 1] = k
    end

    return list

end

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

minetest.register_on_joinplayer(function(player)

    local name = player:get_player_name()

    if data.player_context[name] == nil then

        data.player_context[name] = {party = nil, invites =  {}, status = 2}

    else

        data.player_context[name].status = 2

        if data.player_context[name].party ~= nil then

            add_nametag_info(name, data.player_context[name].party)
            update_party_formspecs(data.player_context[name].party)

        end

    end

    formspec_temp[name] = {}

end
)

minetest.register_on_leaveplayer(function(player)

    local name = player:get_player_name()

    if data.player_context[name] then

        data.player_context[name].status = 1

        if data.player_context[name].party then

            update_party_formspecs(data.player_context[name].party)

        end

    end

    formspec_temp[name] = nil
end)

minetest.register_chatcommand("p", {
    description = "Send a message to the party chat",
    privs = {interact = true, shout = true},
    func = function(name, param)

        local player_info = data.player_context[name]

        if not player_info.party then
            user_message(name, "You're not in any party!")
            return
        end

        if string.len(param) == 0 then return end

        local party_info = data.party_list[player_info.party]

        local message = {}

        message[#message + 1] = left_name_separator
        message[#message + 1] = player_info.party
        message[#message + 1] = right_name_separator
        message[#message + 1] = " <"
        message[#message + 1] = name
        message[#message + 1] = "> "
        message[#message + 1] = param

        local str = table.concat(message)

        str = minetest.colorize(minetest.rgba(
        party_info.name_color.r,
        party_info.name_color.g,
        party_info.name_color.b),
        str)

        for _,v in pairs(party_info.members) do
            minetest.chat_send_player(v.name, str)
        end

    end,
})

sfinv.register_page(mod_name .. ":parties",
    {
        title = "Parties",
        get = get_party_formspec,
        on_player_receive_fields = formspec_action,
    }
)

minetest.register_node(mod_name .. ":party_chest", {
    --TODO: add other fields
    description = "Party Chest",
    paramtype = "light",

    on_construct = function(pos)

        local meta = minetest.get_meta(pos)
        meta:set_string("infotext", "Party Chest")

    end,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)

        if not clicker then return end

        local name = clicker:get_player_name()

        if not name then return end

        local party_name = parties.get_player_party(name)

        if not party_name then
            user_message(name, "You're not in any party!")
            return
        end

        local formspec = {}

        formspec[#formspec + 1] = "size[8,10.25]"
        formspec[#formspec + 1] = default.gui_bg
        formspec[#formspec + 1] = default.gui_bg_img
        formspec[#formspec + 1] = default.gui_slots
        formspec[#formspec + 1] = "list["
        formspec[#formspec + 1] = "detached:"
        formspec[#formspec + 1] = mod_name
        formspec[#formspec + 1] = party_name
        formspec[#formspec + 1] = ";main;0,0;8,6;]"
        formspec[#formspec + 1] = "list[current_player;main;0,6.5;8,4;]"

        minetest.show_formspec(name,
        mod_name .. "_party_chest", table.concat(formspec))

    end,
})
