-------------------------------------------------
--Default data structures
-------------------------------------------------

simple_climate = {}
local weathers = {}
local player_context = {}
local weather_context = {}

--simple_climate.register_weather()
--simple_climate.get_zone_at_pos()
--simple_climate.start_weather()
--simple_climate.stop_weather()

--weathers["name"] = {def1, def2}
--player_context["name"] =  {ref = player, weather = nil, snd_handles = {}, eff_handles = {}}
--weather_context[zone] = {current = nil, timer = 0, possible = {}}

local temps = {"cold", "temperate", "hot"}
local hums = {"low", "medium", "high"}
local modname = "simple_climate"
--TODO: replace hardcoded strings with modname
--TODO: fix zone change cycle delay
local default_sky = {
    type = "regular",
    clouds = true,
    sky_color = {
        day_sky = "#8cbafa",
        day_horizon = "#9bc1f0",
        dawn_sky = "#b4bafa",
        dawn_horizon = "#bac1f0",
        night_sky = "#006aff",
        night_horizon = "#4090ff",
        indoors = "#646464",
        fog_tint_type = "default",
    },
}

local default_clouds = {
    density = 0.4,
    color = "#fff0f0e5",
    ambient = "#000000",
    height = 120,
    thickness = 16,
    speed = {x = 0, z = -2},
}

-------------------------------------------------
--Settingtype initialization
-------------------------------------------------

local step_delay = tonumber(minetest.settings:get("")) or 3
local t_low_edge = tonumber(minetest.settings:get("")) or 30
local t_high_edge = tonumber(minetest.settings:get("")) or 75
local h_low_edge = tonumber(minetest.settings:get("")) or 30
local h_high_edge = tonumber(minetest.settings:get("")) or 75
local weather_chance = tonumber(minetest.settings:get("")) or 1
local y_min = tonumber(minetest.settings:get("")) or -64
local y_max = tonumber(minetest.settings:get("")) or 256
local debug = minetest.settings:get_bool("simple_climate_debug", true)
local sound_fade_interval = 2

-------------------------------------------------
--Utils
-------------------------------------------------

local function index_zone(x, y)
    return ((y - 1) * 3) + x
end

for t = 1,3 do
    for h = 1,3 do
        weather_context[index_zone(t,h)] = {current = nil, timer = 0, possible = {}}
    end
end

local function table_contains(tbl, elem)
    for _,v in pairs(tbl) do
        if v == elem then return true end
    end
    return false
end

simple_climate.get_zone_at_pos = function(pos)

    local t = minetest.get_heat(pos)
    local h = minetest.get_humidity(pos)

    local rt = 1
    local rh = 1

    if t > t_low_edge then
        rt = 2
    end
    if t > t_high_edge then
        rt = 3
    end

    if h > h_low_edge then
        rh = 2
    end
    if h > h_high_edge then
        rh = 3
    end

    return index_zone(rt,rh)
end

simple_climate.register_weather = function(name, def)

    weathers[name] = def

    if def.occurs_naturally then

        for t = 1,3 do
            for h = 1,3 do
               if table_contains(def.heat, temps[t]) and
                table_contains(def.humidity, hums[h]) then
                    table.insert(weather_context[index_zone(t,h)].possible, name)
                end
            end
        end

    end
end

local function test_table_empty(input)

    for _,_ in pairs(input) do
        return false
    end

    return true
end

local function debug_msg(level, msg)
    if debug then
        minetest.log(level, "[" .. modname .. "] " .. msg)
    end
end

-------------------------------------------------
--Weather context and zone handling
-------------------------------------------------

simple_climate.start_weather = function(zone, weather)
    local def = weathers[weather]
    if def then
        weather_context[zone].current = weather
        weather_context[zone].timer = def.duration + math.random(0, def.random_duration)
        debug_msg("info", "Starting " .. weather .. " for zone " .. zone)
    else
        debug_msg("error", "Weather " .. weather .. " is not registered!")
    end
end

simple_climate.stop_weather = function(zone)
    weather_context[zone].current = nil
    weather_context[zone].timer = 0
end

local function update_zones(dtime)

    if test_table_empty(weathers) then return end

    for k,v in pairs(weather_context) do

        if #v.possible == 0 then return end

        if v.current and v.timer > 0 then

            v.timer = v.timer - dtime

        elseif v.current and v.timer <= 0 then

            if weathers[v.current].follows_with and
            weathers[v.current].follow_chance > math.random() then
                local new = weathers[v.current].follows_with[math.random(1, #weathers[v.current].follows_with)]
                simple_climate.start_weather(k, new)
            else
                simple_climate.stop_weather(k)
            end

        elseif not v.current then

            if weather_chance > math.random() then
                local new = v.possible[math.random(1, #v.possible)]
                simple_climate.start_weather(k, new)
            end

        end

    end
end

-------------------------------------------------
--Player context and effects handling
-------------------------------------------------

local function stop_effects(name, ref)

    local eff_handles = player_context[name].eff_handles
    local snd_handles = player_context[name].snd_handles

    if #eff_handles ~= 0 then
        for _,v in pairs(eff_handles) do
            minetest.delete_particlespawner(v)
        end
        player_context[name].eff_handles = {}
    end

    if #snd_handles ~= 0 then
        for _,v in pairs(snd_handles) do
            --TODO: add sound_fade()
            minetest.sound_stop(v)
        end
        player_context[name].snd_handles = {}
    end

    player_context[name].weather = nil

    ref:set_sky(default_sky)
    ref:set_clouds(default_clouds)

end

local function start_effects(name, ref, weather)

    print("starting", name, weather)

    stop_effects(name, ref)

    local def = weathers[weather]

    if def.particle_defs then

        local eff_handles = player_context[name].eff_handles

        for _,v in pairs(def.particle_defs) do
            local handle = minetest.add_particlespawner({
                amount = v.amount,
                time = 0,
                minpos = v.minpos,
                maxpos = v.maxpos,
                minvel = v.minvel,
                maxvel = v.maxvel,
                minacc = v.minacc,
                maxacc = v.maxacc,
                minexptime = v.minexptime,
                maxexptime = v.maxexptime,
                minsize = v.minsize,
                maxsize = v.maxsize,
                collisiondetection = v.collisiondetection,
                collision_removal = v.collision_removal,
                object_collision = v.object_collision,
                attached = ref,
                vertical = v.vertical,
                playername = name,
                animation = v.animation,
                glow = v.glow,
                texture = v.texture,
            })
            eff_handles[#eff_handles + 1] = handle
        end

    end

    if def.loop_sounds then

        local snd_handles = player_context[name].snd_handles

        for k,v in pairs(def.loop_sounds) do
            local handle = minetest.sound_play(v.sound,
            {
                to_player = ref,
                loop = true,
                gain = v.gain,
                pitch = v.pitch
            })
            snd_handles[#snd_handles + 1] = handle
        end

    end

    print("setclimate")
    if def.sky_parameters then
        ref:set_sky(def.sky_parameters)
    end

    if def.cloud_parameters then
        ref:set_clouds(def.cloud_parameters)
    end

    player_context[name].weather = weather

end

local function play_random_sounds(player, weather)

    local def = weathers[weather]
    if not def.random_sounds then return end
    if math.random() > def.random_sound_chance then return end

    local sound = def.random_sounds[math.random(1, #def.random_sounds)]
    minetest.sound_play(sound.sound,
        {
            to_player = player,
            gain = sound.gain,
            pitch = sound.pitch,
        },
        true)
end

local function on_step_callback(ref, current)
    if current and weathers[current].on_step then
        weathers[current].on_step(ref, current)
    end
end

local function on_enter_callback(ref, current)
    if current and weathers[current].on_enter then
        weathers[current].on_enter(ref, current)
    end

end

local function on_leave_callback(ref, current)
    if current and weathers[current].on_leave then
        weathers[current].on_leave(ref, current)
    end
end

local function update_players()

    if test_table_empty(weathers) then
        return
    end

    for k,v in pairs(player_context) do

        local pos = v.ref:get_pos()

        if pos.y < y_max and pos.y > y_min then

            local zone = simple_climate.get_zone_at_pos(pos)
            local current = weather_context[zone].current

            print(k, zone, current)

            if current then

                if not v.weather then

                    start_effects(k, v.ref, current)
                    on_enter_callback(v.ref, current)

                elseif v.weather ~= current then

                    start_effects(k, v.ref, current)
                    on_leave_callback(v.ref, v.weather)
                    on_enter_callback(v.ref, current)

                end

                play_random_sounds(v.ref, current)

            elseif v.weather then

                stop_effects(k, v.ref)
                on_leave_callback(v.ref, v.weather)

            end

        else

            if v.weather then
                stop_effects(k, v.ref)
                on_leave_callback(v.ref, v.weather)
            end

        end

    end

end

-------------------------------------------------
--Registrations
-------------------------------------------------

minetest.register_on_joinplayer(function(player)
    local name = player:get_player_name()
    player_context[name] = {ref = player, weather = nil, snd_handles = {}, eff_handles = {}}
end)

minetest.register_on_leaveplayer(function(player)
    local name = player:get_player_name()
    player_context[name] = nil
end)

-- minetest.register_privilege("weather", {
--     description = "Allows to control weather",
--     give_to_singleplayer = false,
--     give_to_admin = true,
-- })
--
-- minetest.register_chatcommand("setweather", {
--     params = "<weather> <zone>",
--     description = "",
--     privs = {privs=true},  -- Require the "privs" privilege to run
--
--         func = function(name, param),
--         Called when command is run. Returns boolean success and text output.
--     }


-------------------------------------------------
--Main loop
-------------------------------------------------

local timer = 0
minetest.register_globalstep(function(dtime)
    timer = timer + dtime
    if timer < step_delay then return end
    update_zones(step_delay + dtime)
    update_players()
    timer = 0
end)
