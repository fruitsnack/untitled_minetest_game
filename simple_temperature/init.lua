-------------------------------------------------------------------------------
--Default data structures
-------------------------------------------------------------------------------
--TODO: register test nodes and debug liquid and env node temperatures
simple_temperature = {}

local player_context = {}
local liquid_nodes = {}
local temperature_nodes = {}

-------------------------------------------------------------------------------
--Internal variables
-------------------------------------------------------------------------------

local height_factor = 3
local hudbars_enabled = false
local modname = "simple_temperature"
local enabled = true
local temp_min = 0
local temp_max = 100

-------------------------------------------------------------------------------
--Settingtype initialization
-------------------------------------------------------------------------------

local function get_setting(name, def, bool)

    if bool then
        local inp = minetest.settings:get_bool(modname .. "_" .. name, def)
        return inp
    else
        local inp = minetest.settings:get(modname .. "_" .. name) or def
        return tonumber(inp)
    end

end

local interval = get_setting("interval", 3)
local low_temp_threshold = get_setting("low_damage", 10)
local high_temp_threshold = get_setting("high_damage", 90)
local biome_temp_y_min = get_setting("biome_temp_y_min", -64)
local biome_temp_y_max = get_setting("biome_temp_y_max", 256)
local height_temp_y_min = get_setting("height_temp_y_min", -31000)
local height_temp_y_max = get_setting("height_temp_y_max", 31000)
local temp_at_max_height = get_setting("temp_at_y_max", -40)
local temp_at_min_height = get_setting("temp_at_y_min", 50)
local shelter_height = get_setting("shelter_height", 16)
local change_multiplier = get_setting("change_multiplier", 0.5)
local damage_per_step = get_setting("damage", 1)
local node_radius = get_setting("node_radius", 5)

-------------------------------------------------------------------------------
--Mod integrations
-------------------------------------------------------------------------------

if minetest.get_modpath("hudbars") then
    hb.register_hudbar(
        "temperature",
        0xFFFFFF,
        "Temperature",
        {
            bar = "simple_temperature_bar.png",
            icon = "simple_temperature_icon.png",
            bgicon = "simple_temperature_bg.png"
        },
        temp_min,
        temp_max,
        false)
    hudbars_enabled = true
end

-------------------------------------------------------------------------------
--Utilities
-------------------------------------------------------------------------------

local function msg(level, input)
    minetest.log(level, "[" .. modname .. "] " .. input)
end

local function fastpow(a, b)
    local state = a
    for i = 1, b - 1 do
        state = state * a
    end
    return state
end

local function fastabs(a)
    if a >= 0 then
        return a
    else
        return -a
    end
end

local function distance(pos1, pos2)
    return math.sqrt(
    fastpow(pos1.x - pos2.x, 2) +
    fastpow(pos1.y - pos2.y, 2) +
    fastpow(pos1.z - pos2.z, 2)
    )
end

local function is_sheltered(pos)

    for i = 1,shelter_height do
        local node = minetest.get_node_or_nil({
            x = pos.x,
            y = pos.y + i,
            z = pos.z
        })
        if node and node.name ~= "air" and node.walkable then
            return true
        end
    end

    return false

end

local function get_heat_multiplier(t, h)
    return 1 - math.sqrt(((((t/temp_max)-0.5)*((h/temp_max)-0.5))+0.25)*2)
end

local function get_height_temperature(y)
    if y > temp_min then
        local factor = math.min(y/fastabs(biome_temp_y_max), 1)
        return fastpow(fastabs(y/height_temp_y_max), height_factor)
        * temp_at_max_height * factor
    else
        local factor = math.min(y/fastabs(biome_temp_y_min), 1)
        return fastpow(fastabs(y/height_temp_y_min), height_factor)
        * temp_at_min_height * factor
    end
end

local function get_node_env_temperature(pos)
    local t = 0
    for k,v in pairs(temperature_nodes) do
        local nodepos = minetest.find_node_near(pos, node_radius, k, true)
        if nodepos then
            t = t + (v / math.max(distance(pos, nodepos), 1))
        end
    end
    return t
end

-------------------------------------------------------------------------------
--API
-------------------------------------------------------------------------------

simple_temperature.register_liquid_node = function(name, add_temp)
    if not liquid_nodes[name] then
        liquid_nodes[name] = add_temp
    else
        msg("error", "Node " .. name .. " is already registered!")
    end
end

simple_temperature.register_temperature_node = function(name, add_temp)
    if not temperature_nodes[name] then
        temperature_nodes[name] = add_temp
    else
        msg("error", "Node " .. name .. " is already registered!")
    end
end

simple_temperature.get_stats_at = function(pos)

    if not enabled then
        return nil
    end

    local node = minetest.get_node_or_nil(pos)
    local stats = minetest.get_biome_data(pos)

    local target_heat = 50
    local heat_multiplier = 1

    if pos.y > biome_temp_y_min and pos.y < biome_temp_y_max
    and not is_sheltered(pos) then
        target_heat = stats.heat
        heat_multiplier = get_heat_multiplier(stats.heat, stats.humidity)
    end

    if node and liquid_nodes[node.name] then
        target_heat = target_heat + liquid_nodes[node.name]
        heat_multiplier = 1
    end

    target_heat = target_heat + get_height_temperature(pos.y) +
    get_node_env_temperature(pos)

    target_heat = math.max(temp_min, math.min(temp_max, target_heat))

    return {heat = target_heat, heat_multiplier = heat_multiplier}
end

-------------------------------------------------------------------------------
--Starting checks
-------------------------------------------------------------------------------

if not minetest.get_biome_data({x=0, y=0, z=0}) then
    msg("error", "Failed to get biome data!")
    enabled = false
    return
end

-------------------------------------------------------------------------------
--Registrations
-------------------------------------------------------------------------------

if enabled then
    minetest.register_on_joinplayer(function(player)
        local name = player:get_player_name()
        local stats = minetest.get_biome_data(player:get_pos())
        player_context[name] = {ref = player, temperature = stats.heat}
        if hudbars_enabled then
            hb.init_hudbar(player,
            "temperature",
            player_context[name].temperature,
            100)
        end
    end)

    minetest.register_on_leaveplayer(function(player)
        local name = player:get_player_name()
        player_context[name] = nil
    end)
end

-------------------------------------------------------------------------------
--Globalstep
-------------------------------------------------------------------------------

local function update_players()

    for k,v in pairs(player_context) do

        local stats = simple_temperature.get_stats_at(v.ref:get_pos())

        v.temperature = v.temperature + ((stats.heat - v.temperature) *
        stats.heat_multiplier * change_multiplier)

        if v.temperature > high_temp_threshold or
        v.temperature < low_temp_threshold then
            if v.ref:get_hp() > 0 then
                v.ref:set_hp(math.max(0, v.ref:get_hp() - damage_per_step))
            end
        end

        if hudbars_enabled then
            hb.change_hudbar(v.ref, "temperature", v.temperature)
        end

    end
end

local timer = 0
if enabled then
    minetest.register_globalstep(function(dtime)
        timer = timer + dtime
        if timer < interval then return end
        update_players()
        timer = 0
    end)
end

-------------------------------------------------------------------------------
--Node registrations
-------------------------------------------------------------------------------

dofile(minetest.get_modpath(modname) .. "/registrations.lua")
