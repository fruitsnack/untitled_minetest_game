--------------------------------------------------------------------------------
--Cold Liquid nodes
--------------------------------------------------------------------------------

simple_temperature.register_liquid_node("default:water_source", -25)
simple_temperature.register_liquid_node("default:water_flowing", -25)

--------------------------------------------------------------------------------
--Hot liquid nodes
--------------------------------------------------------------------------------

simple_temperature.register_liquid_node("default:lava_source", 100)
simple_temperature.register_liquid_node("default:lava_flowing", 100)

--------------------------------------------------------------------------------
--Hot radiant nodes
--------------------------------------------------------------------------------

simple_temperature.register_temperature_node("default:torch", 25)
simple_temperature.register_temperature_node("fire:basic_flame", 45)
simple_temperature.register_temperature_node("fire:permanent_flame", 45)
